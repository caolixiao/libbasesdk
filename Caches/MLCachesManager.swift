//
//  MLCachesManager.swift
//

import Foundation

public struct MLCachesNotification: Equatable {
    
    public struct Name : RawRepresentable, Equatable, Hashable {
        public var rawValue: String
        
        public init(_ rawValue: String) {
            self.rawValue = rawValue
        }
        
        public init(rawValue: String) {
            self.rawValue = rawValue
        }
    }
    
    public var name: MLCachesNotification.Name
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.name == rhs.name
    }
}

public class MLCachesManager: NSObject {
    
    public static let shared = MLCachesManager()
    public var path: String { return _cachesFilePath }
    
    private static let CACHES_BOUNDLE: String = "/ml/caches/"
    private let _ext: String = ".ml"
    private var _cachesFilePath: String = ""
    
    private override init() {
        super.init()
        
        _cachesFilePath = APP_PATH + MLCachesManager.CACHES_BOUNDLE
        
        do {
            if(!FileManager.default.fileExists(atPath: _cachesFilePath)) {
                try FileManager.default.createDirectory(atPath: _cachesFilePath, withIntermediateDirectories: true, attributes: nil)
            }
        } catch {
            Log.i("carete caches file fail ------- \(_cachesFilePath)")
        }
    }
    
    /// 路径转化
    fileprivate func filePath(_ atPath: MLCachesNotification.Name) -> String {
        let descPath = _cachesFilePath + atPath.rawValue + _ext
        let path = descPath.deletingLastPathComponent
        
        do {
            if (!FileManager.default.fileExists(atPath: path)) {
                try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            }
        } catch {
            Log.i("carete caches file fail ------- \(path)")
        }
        
        return descPath
    }
    
    /// 删除所有缓冲
    public func removeAll() {
        do {
            try FileManager.default.removeItem(atPath: _cachesFilePath)
            try FileManager.default.createDirectory(atPath: _cachesFilePath, withIntermediateDirectories: true, attributes: nil)
        } catch {
            Log.i("remove caches file fail ------- \(_cachesFilePath)")
        }
    }
    
    /// 删除记录
    public func remove(_ name: MLCachesNotification.Name) {
        do {
            let filePath = self.filePath(name)
            try FileManager.default.removeItem(atPath: filePath)
        } catch { }
    }
}

/// 保存dict
extension MLCachesManager {
    /// 保存记录
    public func save(_ name: MLCachesNotification.Name, dict: [AnyHashable : Any]?) {
        guard let val = dict else{ return }
        let tmp = val.filter({($0.value as? NSNull) == nil})
        guard let mData = tmp as NSDictionary? else { return }
        
        let filePath = self.filePath(name)
        mData.write(toFile: filePath, atomically: false)
    }
    
    /// 修改记录
    public func update(_ name: MLCachesNotification.Name, dict : [AnyHashable : Any]?) {
        save(name, dict: dict)
    }
    
    /// 获取记录
    public func find(dictOfName: MLCachesNotification.Name) -> [AnyHashable : Any]? {
        let filePath = self.filePath(dictOfName)
        guard let dict = NSDictionary(contentsOfFile: filePath) as? [AnyHashable : Any] else {
            return nil
        }
        return dict
    }
}

/// 保存 Array
extension MLCachesManager {
    /// 保存记录
    public func save(_ name: MLCachesNotification.Name, array: [Any]?) {
        guard let val = array else{ return }
//        let tmp = val.filter({($0.value as? NSNull) == nil})
        guard let mArray = val as NSArray? else { return }
        
        let filePath = self.filePath(name)
        mArray.write(toFile: filePath, atomically: false)
    }
    
    /// 修改记录
    public func update(_ name: MLCachesNotification.Name, array: [Any]?) {
        save(name, array: array)
    }
    
    /// 获取记录
    public func find(arrayOfName: MLCachesNotification.Name) -> [Any]? {
        let filePath = self.filePath(arrayOfName)
        guard let array = NSArray(contentsOfFile: filePath) as? [Any] else {
            return nil
        }
        return array
    }
}

/// 保存 String
extension MLCachesManager {
    /// 保存记录
    public func save(_ name: MLCachesNotification.Name, str: String?) {
        guard let val = str else{ return }
        
        do {
            let filePath = self.filePath(name)
            try val.write(toFile: filePath, atomically: false, encoding: String.Encoding.utf8)
        } catch { }
    }
    
    /// 修改记录
    public func update(_ name: MLCachesNotification.Name, str : String?) {
        save(name, str: str)
    }
    
    /// 获取记录
    public func find(strOfName: MLCachesNotification.Name) -> String? {
        do {
            let filePath = self.filePath(strOfName)
            guard let val = try String(contentsOfFile: filePath) as? String else {
                return nil
            }
            
            return val
        } catch { return nil }
    }
}

/// 保存对象
extension MLCachesManager {
    /// 保存记录
    public func save<T: MLBaseModel>(_ name: MLCachesNotification.Name, data: T) {
        guard let val = data.toJSON() else { return }
        let tmp = val.filter({($0.value as? NSNull) == nil})
        guard let dict = tmp as NSDictionary? else { return }
        
        let filePath = self.filePath(name)
        dict.write(toFile: filePath, atomically: false)
    }
    
    
    /// 修改记录
    public func update<T: MLBaseModel>(_ name: MLCachesNotification.Name, data: T) {
        save(name, data: data)
    }
    
    /// 获取记录
    public func find<T: MLBaseModel>(name: MLCachesNotification.Name) -> T? {
        let filePath = self.filePath(name)
        guard let dict = NSDictionary(contentsOfFile: filePath) as? [String : Any] else {
            return nil
        }
        return T.deserialize(from: dict)
    }
}

/// 保存文件
extension MLCachesManager {
    /// 保存记录
    @discardableResult
    public func save(fileName: MLCachesNotification.Name, data: Data) -> String {
        let filePath = self.filePath(fileName)
        FileManager.default.createFile(atPath: filePath, contents: data, attributes: nil)
        return filePath
    }
    
    /// 删除记录
    public func remove(fileName: MLCachesNotification.Name) {
        do {
            let filePath = self.filePath(fileName)
            try FileManager.default.removeItem(atPath: filePath)
        } catch { }
    }
    
    /// 修改记录
    public func update(fileName: MLCachesNotification.Name, data: Data) {
        remove(fileName: fileName)
        save(fileName: fileName, data: data)
    }
    
    /// 获取记录
    public func find(filePath fileName: MLCachesNotification.Name) -> String? {
        let filePath = self.filePath(fileName)
        if FileManager.default.fileExists(atPath: filePath) {
            return filePath
        }
        return nil
    }
    
    /// 获取记录
    public func find(fileData fileName: MLCachesNotification.Name) -> Data? {
        let filePath = self.filePath(fileName)
        if FileManager.default.fileExists(atPath: filePath) {
            do {
                return try Data.init(contentsOf: URL(fileURLWithPath: filePath))
            } catch { }
        }
        return nil
    }
}
