//
//  MLAppDelegateService.swift
//

import UIKit


/// 返回结果方式只会返回第一个的
open class MLAppDelegateService: UIResponder, MLApplicationDelegate {
    
    public var services: [MLApplicationDelegate] = []
    public var window: UIWindow?
    
    public override init() {
        super.init()
        services = loadServiceClass()
    }
    
    open func loadServiceClass() -> [MLApplicationDelegate] {
        return []
    }
}

extension MLAppDelegateService {
    public class var shared: MLAppDelegateService? {
        get {
            return UIApplication.shared.delegate as? MLAppDelegateService
        }
    }
    
    public class func makeKeyAndVisible() {
        if let app = UIApplication.shared.delegate as? MLAppDelegateService {
            app.window?.makeKeyAndVisible()
        }
    }
    
    public func viewController() -> UIViewController? {
        if let app = UIApplication.shared.delegate as? MLAppDelegateService {
            return app.window?.currentViewController()
        }
        return nil
    }
}

// MARK: -
extension MLAppDelegateService {
    
    public func applicationDidFinishLaunching(_ application: UIApplication) {
        services.forEach { $0.applicationDidFinishLaunching?(application) }
    }
    
    public func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, willFinishLaunchingWithOptions: launchOptions) } as? Bool else { return false }
        return ret
    }
    
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, didFinishLaunchingWithOptions: launchOptions) } as? Bool else { return false }
        return ret
    }
    
    public func applicationDidBecomeActive(_ application: UIApplication) {
        services.forEach { $0.applicationDidBecomeActive?(application) }
    }
    
    public func applicationWillResignActive(_ application: UIApplication) {
        services.forEach { $0.applicationWillResignActive?(application) }
    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        guard let ret = services.forEachRet { _ = $0.application?(app, open: url, options: options) } as? Bool else { return false }
        return ret
    }
    
    public func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        services.forEach { $0.applicationDidReceiveMemoryWarning?(application) }
    }
    
    public func applicationWillTerminate(_ application: UIApplication) {
        services.forEach { $0.applicationWillTerminate?(application) }
    }
    
    public func applicationSignificantTimeChange(_ application: UIApplication) {
        services.forEach { $0.applicationSignificantTimeChange?(application) }
    }
    
    @available(iOS, introduced: 2.0, deprecated: 13.0, message: "Use viewWillTransitionToSize:withTransitionCoordinator instead")
    public func application(_ application: UIApplication, willChangeStatusBarOrientation newStatusBarOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        services.forEach { $0.application?(application, willChangeStatusBarOrientation: newStatusBarOrientation, duration: duration) }
    }
    
    @available(iOS, introduced: 2.0, deprecated: 13.0, message: "Use viewWillTransitionToSize:withTransitionCoordinator instead")
    public func application(_ application: UIApplication, didChangeStatusBarOrientation oldStatusBarOrientation: UIInterfaceOrientation) {
        services.forEach { $0.application?(application, didChangeStatusBarOrientation: oldStatusBarOrientation) }
    }
    
    @available(iOS, introduced: 2.0, deprecated: 13.0, message: "Use viewWillTransitionToSize:withTransitionCoordinator instead")
    public func application(_ application: UIApplication, willChangeStatusBarFrame newStatusBarFrame: CGRect) {
        services.forEach { $0.application?(application, willChangeStatusBarFrame: newStatusBarFrame) }
    }
    
    @available(iOS, introduced: 2.0, deprecated: 13.0, message: "Use viewWillTransitionToSize:withTransitionCoordinator instead")
    public func application(_ application: UIApplication, didChangeStatusBarFrame oldStatusBarFrame: CGRect) {
        services.forEach { $0.application?(application, didChangeStatusBarFrame: oldStatusBarFrame) }
    }
    
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        services.forEach { $0.application?(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken) }
    }
    
    public func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        services.forEach { $0.application?(application, didFailToRegisterForRemoteNotificationsWithError: error) }
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        services.forEach { $0.application?(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler) }
    }
    
    //BGAppRefreshTask in the BackgroundTasks framework instead
    //    public func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    //        services.forEach{ $0.application?(application, performFetchWithCompletionHandler: completionHandler) }
    //    }
    
    public func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        services.forEach{ $0.application?(application, performActionFor: shortcutItem, completionHandler: completionHandler) }
    }
    
    public func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        services.forEach{ $0.application?(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler) }
    }
    
    public func application(_ application: UIApplication, handleWatchKitExtensionRequest userInfo: [AnyHashable : Any]?, reply: @escaping ([AnyHashable : Any]?) -> Void) {
        services.forEach { $0.application?(application, handleWatchKitExtensionRequest: userInfo, reply: reply) }
    }
    
    public func applicationShouldRequestHealthAuthorization(_ application: UIApplication) {
        services.forEach { $0.applicationShouldRequestHealthAuthorization?(application) }
    }
    
    //- (void)application:(UIApplication *)application handleIntent:(INIntent *)intent completionHandler:(void(^)(INIntentResponse *intentResponse))completionHandler API_AVAILABLE(ios(11.0));
    
    public func applicationWillEnterForeground(_ application: UIApplication) {
        services.forEach { $0.applicationWillEnterForeground?(application) }
    }
    
    public func applicationDidEnterBackground(_ application: UIApplication) {
        services.forEach { $0.applicationDidEnterBackground?(application) }
    }
    
    public func applicationProtectedDataWillBecomeUnavailable(_ application: UIApplication) {
        services.forEach { $0.applicationProtectedDataWillBecomeUnavailable?(application) }
    }
    
    public func applicationProtectedDataDidBecomeAvailable(_ application: UIApplication) {
        services.forEach { $0.applicationProtectedDataDidBecomeAvailable?(application) }
    }
    
    public func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        guard let ret = services.forEachRet { $0.application?(application, supportedInterfaceOrientationsFor: window) } as? UIInterfaceOrientationMask else { return .portrait }
        return ret
    }
    
    public func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplication.ExtensionPointIdentifier) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, shouldAllowExtensionPointIdentifier: extensionPointIdentifier) } as? Bool else { return false }
        return ret
    }
    
    //#pragma mark -- State Restoration protocol adopted by UIApplication delegate --
    public func application(_ application: UIApplication, viewControllerWithRestorationIdentifierPath identifierComponents: [String], coder: NSCoder) -> UIViewController? {
        return services.forEach { $0.application?(application, viewControllerWithRestorationIdentifierPath: identifierComponents, coder: coder) } as? UIViewController
    }
    
    public func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, shouldSaveApplicationState: coder) } as? Bool else { return false }
        return ret
    }
    
    public func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, shouldRestoreApplicationState: coder) } as? Bool else { return false }
        return ret
    }
    
    public func application(_ application: UIApplication, willEncodeRestorableStateWith coder: NSCoder) {
        services.forEach { $0.application?(application, willEncodeRestorableStateWith: coder) }
    }
    
    public func application(_ application: UIApplication, didDecodeRestorableStateWith coder: NSCoder) {
        services.forEach { $0.application?(application, didDecodeRestorableStateWith: coder) }
    }
    
    @available(iOS 13.2, *)
    public func application(_ application: UIApplication, shouldSaveSecureApplicationState coder: NSCoder) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, shouldSaveSecureApplicationState: coder) } as? Bool else { return false }
        return ret
    }
    
    @available(iOS 13.2, *)
    public func application(_ application: UIApplication, shouldRestoreSecureApplicationState coder: NSCoder) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, shouldRestoreSecureApplicationState: coder) } as? Bool else { return false }
        return ret
    }
    
    //#pragma mark -- User Activity Continuation protocol adopted by UIApplication delegate --
    public func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, willContinueUserActivityWithType: userActivityType) } as? Bool else { return false }
        return ret
    }
    
    public func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard let ret = services.forEachRet { $0.application?(application, continue: userActivity, restorationHandler: restorationHandler) } as? Bool else { return false }
        return ret
    }
    
    public func application(_ application: UIApplication, didFailToContinueUserActivityWithType userActivityType: String, error: Error) {
        services.forEach { $0.application?(application, didFailToContinueUserActivityWithType: userActivityType, error: error) }
    }
    
    public func application(_ application: UIApplication, didUpdate userActivity: NSUserActivity) {
        services.forEach { $0.application?(application, didUpdate: userActivity) }
    }
    
    
    //#pragma mark -- CloudKit Sharing Invitation Handling --
    //    func applicatio
    
    //#pragma mark -- UIScene Support --
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //
    //    }
    
    
}
