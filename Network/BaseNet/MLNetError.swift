//
//  MLNetError.swift
//

import UIKit

public enum MLNetError: Error {
    case deserialize_error(Any?)
    case result_nil_error(String?)
    case skip_error(String?)
}

extension MLNetError {
    var localizedDescription: String {
        switch self {
        case .deserialize_error(let val):
            return "ml_net_error => deserialize_error \(val)."
        case .result_nil_error(let val):
            return "ml_net_error => result_nil_error \(val)."
        case .skip_error(let str):
            return "ml_net_error => skip_error \(str)."
        }
    }
}
