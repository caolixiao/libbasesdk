//
//  MLNetBaseManager.swift
//

import Foundation
import UIKit

public enum MLNetResponseType {
    case json
    case data
    case string
}

public enum MLNetRequestType {
    case json
    case from
    case xml
}

public enum MLNetRespCodeType: Int {
    case string
    case int
}

open class MLNetBaseManager: NSObject {
    
    fileprivate let kUserDefaultsToken = "kUserDefaultsToken"
    
    public var url: String?
    public var token: String?
    public var devInfo = [AnyHashable: AnyObject]()
    
    public required override init () {
        super.init()
        
        devInfo["uuid"]     =  DEVICE_UDID as AnyObject?
        devInfo["version"]  = VERSION as AnyObject?
        devInfo["osv"]      = DEVICE_SYSTEAM_VERSION as AnyObject?
        devInfo["model"]    = DEVICE_MODEL as AnyObject?
        devInfo["screen"]   = __SCREEN_DISPLAY as AnyObject?
        devInfo["density"]  = __SCREEN_SCALE as AnyObject?
        
        if let val = UserDefaults.standard.object(forKey: kUserDefaultsToken) as? String { token = val }
    }
    
    public func setToken(value: String) {
        token = value;
        UserDefaults.standard.set(value, forKey: kUserDefaultsToken)
        UserDefaults.standard.synchronize()
    }
}

public typealias HTTPHeaders = [String: String]

@objc
public protocol MLNetManagerDelegate: NSObjectProtocol {
    
    /// 基础信息
    @objc
    @available(iOS 9.0, *)
    optional func loadBaseURL(_ manager: MLNetBaseManager) -> URL
    
    
    @available(iOS 9.0, *)
    func loadBasePathUrl(_ manager: MLNetBaseManager) -> String
    
    
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, defaultHeaders header: HTTPHeaders?) -> HTTPHeaders
    
    
    /// cookie
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, cachesToCookie cookie:  [HTTPCookie]?) -> Bool
    
    
    /// 过滤
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, didBlackForexclude_urls urls: [String]?) -> [String]
    
    /// 业务返回状态
    @objc
    @available(iOS 9.0, *)
    optional func net(respCodeOfBusiness manager: MLNetBaseManager) -> String
    
    @objc
    @available(iOS 9.0, *)
    optional func net(respCodeTypeOfBusiness manager: MLNetBaseManager) -> MLNetRespCodeType.RawValue
    
    
    /// 状态
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, didFailStatusCode code: Int) -> Bool
    
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, didStatusCode code: Int)
    
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, didCustomStatusCode code: Int)
    
    
    /// 数据
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, data: Any)
    
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, success data: Any)
    
    @objc
    @available(iOS 9.0, *)
    optional func net(_ manager: MLNetBaseManager, fail error: Error?)
}


public func MLNetManagerMain(_ delegateClassName: String?, netManager: MLNetManager? = MLNetManager.shared) -> Int32 {
    guard let className = delegateClassName as? String else {
        assert(false, "MLNetManagerMain delegateClassName not nil")
        return 1
    }
    
    if let type = NSClassFromString(className) as? MLNetBaseManager.Type {
        if let cls = type.init() as? MLNetManagerDelegate {
            netManager?.delegate = cls
            if let domain = netManager?.delegate?.loadBasePathUrl(cls as! MLNetBaseManager) {
                __NET_BASE_DOMAIN =  domain
            }
            else {
                assert(false, "-- 请设置基础的网路地址 --")
            }
        }
    }
    else {
        assert(false, "MLNetManagerMain delegateClassName not extend MLNetBaseManager")
        return 2
    }
     
    return 0
}
