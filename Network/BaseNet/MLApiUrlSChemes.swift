//
//  MLApiUrlSchemes.swift
//

import UIKit
import Foundation
import HandyJSON

//MARK: - 网络基础地址
public var __NET_BASE_DOMAIN = "https://xxx.xxx.xxx"


//MARK: - 声明请求API结构体，包含请求地址和请求名称
public struct MLApiUrlSchemes: ExpressibleByStringLiteral, Equatable {
    
    public typealias RawValue = String
    
    private let apiUrl: String
    
    
    public var stringURL: String { get { return getCreditApiUrl() } }
    public var url: URL { get { return getURL() ?? URL(fileReferenceLiteralResourceName: "") } }
    
    init(_ apiUrl: String) {
        self.apiUrl = apiUrl
    }
    
    public init(stringLiteral api: String) {
        self.init(api)
    }
    
    public init(stringLiteral api: String, val: String) {
        self.init(api)
    }
    
    public static func ==  (lhs: MLApiUrlSchemes, rhs: MLApiUrlSchemes) -> Bool {
        return lhs.apiUrl == rhs.apiUrl
    }
    
    public func getCreditApiUrl() -> String {
        return __NET_BASE_DOMAIN + apiUrl + "?timeStamp=" + String(Date().timeIntervalSince1970)
    }
    
    public func getURL() -> URL? {
        let str = __NET_BASE_DOMAIN + apiUrl + "?timeStamp=" + String(Date().timeIntervalSince1970)
        return URL(string: str)
    }
}


//MARK: - 公共API
public enum MICommonApi: MLApiUrlSchemes {
    case api = "/api"
}


// MARK: - req
public protocol MLReqModel: HandyJSON {
    
}

public struct MLNetReqModel: MLReqModel {
    public init() { }
}


// MARK: - resp
public protocol MLBaseModel: HandyJSON {}
public protocol MLBaseEnum: HandyJSONEnum { }
public protocol MLRespModel: MLBaseModel {
    associatedtype T
    
    var code: Int { get set }
    var msg: String? { get set }
    var data: T? { get set }
     
    var strRaw: String? { get set }
    var dataRaw: Data? { get set }
}
public protocol MLListRespModel: MLBaseModel {
    associatedtype List
    
    var pageNum: Int { get set }
    var pageSize: Int { get set }
    var pages: Int { get set }
    var total: Int { get set }
    var list: List? { get set }
}

// data
public struct MLNetRespModel<T: MLBaseModel>: MLRespModel {
    public init() { }
    public var code: Int = 0
    
    public var msg: String?
    
    public var data: T?
    
    public var strRaw: String?
    
    public var dataRaw: Data?
}

public struct MLNetNotRespModel: MLRespModel {
    public init() { }
    public var code: Int = 0
    
    public var msg: String?
    
    public var data: String?
    
    public var strRaw: String?
    
    public var dataRaw: Data?
}


// list
public struct MLNetPageModel<Value>: MLListRespModel {
    public init() { }
    public typealias List = [Value]
    
    public var pageNum: Int = 0
    public var pageSize: Int = 0
    public var pages: Int = 0
    public var total: Int = 0
    public var list: List?
}

public struct MLNetPageRespModel<T: MLBaseModel>: MLRespModel {
    public init() { }
    public var code: Int = 0
    
    public var msg: String?
    
    public var data: MLNetPageModel<T>?
    
    public var strRaw: String?
    
    public var dataRaw: Data?
}

public struct MLNetListRespModel<T: MLBaseModel>: MLRespModel {
    public init() { }
    public var code: Int = 0
    
    public var msg: String?
    
    public var data: [T]?
    
    public var strRaw: String?
    
    public var dataRaw: Data?
}



