//
//  MLNetManager.swift
//

import Foundation
import UIKit
import Alamofire
import HandyJSON

public class MLNetManager {
    
    // MARK: -
    static public let shared : MLNetManager = { return MLNetManager() }()
    
    //    unowned(unsafe) open var delegate: MLNetManagerDelegate?
    open var delegate: MLNetManagerDelegate?
    
    public var isCookie: Bool {
        get {
            if let _ = MLCachesManager.shared.find(arrayOfName: MLCachesNotification.Name.Auth.Cookie) {
                return true
            }
            return false
        }
    }
    
    // MARK: -
    private let netQueue = DispatchQueue(label: "com.net.thread")
    
    private var _manager: MLNetBaseManager {
        get {
            return delegate as! MLNetBaseManager
        }
    }
    private var _baseManager: MLNetBaseManager? {
        get {
            return delegate as? MLNetBaseManager
        }
    }
    
    // MARK: - 注册代理
    public class func startNetDelegate(_ delegateClassName: String?) {
//        let protectionSpace = URLProtectionSpace.init(host: "39.105.212.234", port: 8080, protocol: "http", realm: nil, authenticationMethod: nil)
//        let userCredential = URLCredential(user: "user", password: "password", persistence: .permanent)
//        URLCredentialStorage.shared.setDefaultCredential(userCredential, for: protectionSpace)
        MLNetManagerMain(delegateClassName)
    }
    
    // MARK: -
    /// 网络请求方法
    public func sendNetRequest<T: MLReqModel, U: MLRespModel>(url: MLApiUrlSchemes, queue: DispatchQueue? = nil, method: HTTPMethod = .get, reqType: MLNetRequestType = .json, respType: MLNetResponseType = .json, paramters: T?, finished: @escaping (_ respModel: U) -> ()) {
        
        let _url = url.getCreditApiUrl()
        _manager.url = _url
        let par = modelToDictionary(paramters)
        let request = alamofireRequest(_url, method: method, reqType: reqType, paramters: par)
        #if DEBUG
        debugPrint(request)
        #endif
        switch respType {
        case .json:
            request.responseJSON(queue: queue, completionHandler: { resp in
                self.json(resp, finished: finished)
            })
            break
        case .data:
            request.responseData(queue: queue, completionHandler: { resp in
                self.data(resp, finished: finished)
            })
            break
        case .string:
            request.responseString(queue: queue, completionHandler: { resp in
                self.string(resp, finished: finished)
            })
            break
        }
    }
    
    private func alamofireRequest(_ url: String, method: HTTPMethod, reqType: MLNetRequestType = .json, paramters: [AnyHashable: Any]) -> DataRequest {
        setCookie(url: url)
        if method == .post || method == .put, var urlRequest = try? URLRequest(url: url, method: method, headers: req_headers()) {
            urlRequest.timeoutInterval = 60
            
            if reqType == .json {
                let data = try? JSONSerialization.data(withJSONObject: paramters, options: [])
                if let data = data {
                    urlRequest.httpBody = data
                }
            }
            else if reqType == .from {
                if let param = paramters as? [String: Any] {
                    return Alamofire.request(url, method: method, parameters: param, encoding: URLEncoding.queryString, headers: req_headers())
                } else {
                    
                }
            }
            else if reqType == .xml {
                if let param = paramters as? [String: Any] {
                    return Alamofire.request(url, method: method, parameters: param, encoding: PropertyListEncoding.xml, headers: req_headers())
                }
            }
            
            return Alamofire.request(urlRequest)
        }
        else if method == .get || method == .head || method == .delete {
            return Alamofire.request(url, parameters: paramters as! Parameters, headers: req_headers())
        }
        return Alamofire.request(url, headers: req_headers())
    }
    
    private func json<U: MLRespModel>(_ resp: DataResponse<Any>, finished: @escaping (_ respModel: U) -> ()) {
        #if DEBUG
        debugPrint(resp)
        #endif
        guard exclude_urls(resp.request?.url?.absoluteString) else { return }
        guard resp_status_code(resp.response, finished: finished) else { return }
        
        var resultsModel = U()
        resp.result.ifSuccess {
            if let dict = resp.value as? [String: Any], var resultsModel = U.deserialize(from: dict) {
                resp_business_code(dict, resp: &resultsModel)
                resp_cookie(resp)
                
                delegate?.net?(_manager, didCustomStatusCode: resultsModel.code)
                delegate?.net?(_manager, success: resultsModel)
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            }
            else if let dict = resp.value as? [[String: Any]] {
                resultsModel.code = 2
                resultsModel.msg = MLNetError.deserialize_error(resp.value).localizedDescription
                delegate?.net?(_manager, fail: MLNetError.deserialize_error(resp.value))
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            }
            else {
                resultsModel.code = -1
                resultsModel.msg = MLNetError.result_nil_error(resp.debugDescription).localizedDescription
                delegate?.net?(_manager, fail: MLNetError.result_nil_error(resp.debugDescription))
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            }
        }
        
        resp.result.ifFailure {
            resultsModel.code = -2
            resultsModel.msg = resp.error?.localizedDescription
            delegate?.net?(_manager, fail: resp.error)
            delegate?.net?(_manager, data: resultsModel)
            finished(resultsModel)
        }
    }
    
    private func data<U: MLRespModel>(_ resp: DataResponse<Data>, finished: @escaping (_ respModel: U) -> ()) {
        #if DEBUG
        debugPrint(resp)
        #endif
        guard resp_status_code(resp.response, finished: finished) else { return }
        
        var resultsModel = U()
        resp.result.ifSuccess {
            if let data = resp.value as? Data {
                resp_cookie(resp)
                
                resultsModel.code = 0
                resultsModel.dataRaw = data
                delegate?.net?(_manager, didCustomStatusCode: resultsModel.code)
                delegate?.net?(_manager, success: resultsModel)
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            } else {
                resultsModel.code = -1
                resultsModel.msg = MLNetError.result_nil_error(resp.debugDescription).localizedDescription
                delegate?.net?(_manager, fail: MLNetError.result_nil_error(resp.debugDescription))
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            }
        }
        
        resp.result.ifFailure {
            resultsModel.code = -2
            resultsModel.msg = resp.error?.localizedDescription
            delegate?.net?(_manager, fail: resp.error)
            delegate?.net?(_manager, data: resultsModel)
            finished(resultsModel)
        }
    }
    
    private func string<U: MLRespModel>(_ resp: DataResponse<String>, finished: @escaping (_ respModel: U) -> ()) {
        #if DEBUG
        debugPrint(resp)
        #endif
        guard resp_status_code(resp.response, finished: finished) else { return }
        
        var resultsModel = U()
        resp.result.ifSuccess {
            if let str = resp.value as? String {
                resp_cookie(resp)
                
                resultsModel.code = 0
                resultsModel.strRaw = str
                delegate?.net?(_manager, didCustomStatusCode: resultsModel.code)
                delegate?.net?(_manager, success: resultsModel)
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            } else {
                resultsModel.code = -1
                resultsModel.msg = MLNetError.result_nil_error(resp.debugDescription).localizedDescription
                delegate?.net?(_manager, fail: MLNetError.result_nil_error(resp.debugDescription))
                delegate?.net?(_manager, data: resultsModel)
                finished(resultsModel)
            }
        }
        resp.result.ifFailure {
            resultsModel.code = -2
            resultsModel.msg = resp.error?.localizedDescription
            delegate?.net?(_manager, fail: resp.error)
            delegate?.net?(_manager, data: resultsModel)
            finished(resultsModel)
        }
    }
    
    // MARK: - tools
    private var UserAgent: String {
        get {
            let mBundle = Bundle.main
            var str = ""
            if let tmp = mBundle.infoDictionary?[String(kCFBundleExecutableKey)] as? String{
                str = tmp
            }
            else if let tmp = mBundle.infoDictionary?[String(kCFBundleIdentifierKey)] as? String{
                str = tmp
            }
            
            var version = mBundle.infoDictionary?[String(kCFBundleVersionKey)] as? String ?? ""
            let device = UIDevice.current
            return "\(str)/\(version) (\(device.model); iOS \(device.systemVersion);Scale/\(String.init(format: "%.2f", UIScreen.main.scale)))"
        }
    }
    
    private func req_headers() -> HTTPHeaders {
        var op: HTTPHeaders =  ["Content-Type": "application/json",
                                "User-Agent": UserAgent]
        
        if let manager = _baseManager {
            for (key, value) in manager.devInfo  { if let k = key as? String, let val = value as? String, !op.keys.contains(k) { op.updateValue(val, forKey: k) } }
            if let token = manager.token as? String { op.updateValue(token, forKey: "token") }
        }
        
        if let header = delegate?.net?(_manager, defaultHeaders: op) as? HTTPHeaders {
            for (key, val) in header  { if !op.keys.contains(key) { op.updateValue(val, forKey: key) } }
        }
        
//        if let cookie = MLCachesManager.shared.find(strOfName: MLCachesNotification.Name.Auth.CookieStr) {
//            op.updateValue(cookie, forKey: "Cookie")
//        }
        
        return op
    }
    
    private func resp_status_code<U: MLRespModel>(_ resp: HTTPURLResponse?, finished: @escaping (_ respModel: U) -> ()) -> Bool {
        if let statusCode = resp?.statusCode as? Int {
            if (statusCode != 200) {
                if delegate?.net?(_manager, didFailStatusCode: statusCode) ?? false {
                    var resultsModel = U()
                    resultsModel.code = 10
                    resultsModel.msg = MLNetError.skip_error(resp?.url?.absoluteString).localizedDescription
                    finished(resultsModel)
                    return false
                }
            }
            delegate?.net?(_manager, didStatusCode: statusCode)
        }
        return true
    }
    
    private func resp_business_code<U: MLRespModel>(_ dict: [String: Any], resp resultsModel: inout U){
        var key: String = "code"
        var type: MLNetRespCodeType? = .int
        if let tmp = delegate?.net?(respCodeOfBusiness: _manager) { key = tmp }
        if let tmp = delegate?.net?(respCodeTypeOfBusiness: _manager) { type = MLNetRespCodeType(rawValue: tmp) }
        
        switch type {
        case .int:
            break
        case .string:
            if let code = dict[key] as? String, code == "SUCCESS" {
                resultsModel.code = 0
            }
            else if let code = dict[key] as? String, code == "UNAUTHORIZED" {
                resultsModel.code = 100
            }
            else if let code = dict["status"] as? Int, let msg = dict["error"] as? String, code != 200 {
                resultsModel.code = code
                resultsModel.msg = msg
            } else {
                resultsModel.code = -1
            }
            break
        default:
            break
        }
    }
    
    
    private func modelToDictionary<T: MLReqModel>(_ reqModel: T?) -> [AnyHashable: Any] {
        if reqModel == nil {
            #if DEBUG
            Log.i("无请求参数")
            #endif
            return [:]
        }
        
        if let val = reqModel?.toJSON() as? [AnyHashable: Any] {
            return val
        }
        return [:]
    }
    
    private func exclude_urls(_ url: String?) -> Bool {
        guard let _url = url else { return true }
        var isExclude = false
        delegate?.net?(_manager, didBlackForexclude_urls: nil).forEach { (excludeUrls) in
            if _url.contains(excludeUrls) {
                isExclude = true
            }
        }
        return !isExclude
    }
    
}

// cookie
extension MLNetManager {
    public func resp_cookie<T: Any>(_ resp: DataResponse<T>) {
        if let isCookie = delegate?.net?(_manager, cachesToCookie: getCookie()) {
            if let headerFields = resp.response?.allHeaderFields as? HTTPHeaders, let url = resp.request?.url {
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
                if cookies.count > 0 {
                    var mACookie = [[HTTPCookiePropertyKey : Any]]()
                    for cookie in cookies {
                        if let val = cookie.properties {
                            mACookie.append(val)
                        }
                    }
                    MLCachesManager.shared.save(MLCachesNotification.Name.Auth.Cookie, array: mACookie)
                }
                if let cookie = headerFields["Set-Cookie"] {
                    MLCachesManager.shared.save(MLCachesNotification.Name.Auth.CookieStr, str: cookie)
                }
            }
        }
    }
        
    private func setCookie(url: String) {
        if let cookies = getCookie() {
            if let url = URL(string: url) {
                SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies, for: url, mainDocumentURL: nil)
            }
        }
    }
    
    public func getCookie() -> [HTTPCookie]? {
        if let mACookie = MLCachesManager.shared.find(arrayOfName: MLCachesNotification.Name.Auth.Cookie) as? [[HTTPCookiePropertyKey : Any]], mACookie.count > 0 {
            var cookies = [HTTPCookie]()
            for mDict in mACookie {
                if let dict = mDict as? [HTTPCookiePropertyKey : Any] {
                    if let cookie = HTTPCookie(properties : dict) {
                        cookies.append(cookie)
                    }
                }
            }
            return cookies
        } else if let mACookie = SessionManager.default.session.configuration.httpCookieStorage?.cookies, mACookie.count > 0 {
            return mACookie
        }
        return nil
    }
    
    public func saveCookie() {
        if let isCookie = delegate?.net?(_manager, cachesToCookie: getCookie()) {
            if let mACookie = SessionManager.default.session.configuration.httpCookieStorage?.cookies, mACookie.count > 0 {
                var cookies = [[HTTPCookiePropertyKey : Any]]()
                for cookie in mACookie {
                    if let val = cookie.properties {
                        cookies.append(val)
                    }
                }
                MLCachesManager.shared.save(MLCachesNotification.Name.Auth.Cookie, array: cookies)
            }
        }
    }

    public func removeCookie() {
        SessionManager.default.session.configuration.httpCookieStorage?.removeCookies(since: Date())
        MLCachesManager.shared.remove(MLCachesNotification.Name.Auth.Cookie)
    }
}

// MARK: -
extension MLNetManager {
    @discardableResult
    public func get<U: MLRespModel>(_ url: MLApiUrlSchemes, resp: MLNetResponseType = .json, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return get(url, resp: resp, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func get<T: MLReqModel, U: MLRespModel>(_ url: MLApiUrlSchemes, resp: MLNetResponseType = .json, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, respType: resp, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func head<U: MLRespModel>(_ url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return head(url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func head<T: MLReqModel, U: MLRespModel>(_ url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, method: .head, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func delete<U: MLRespModel>(_ url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return delete(url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func delete<T: MLReqModel, U: MLRespModel>(_ url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, method: .delete, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T, U>(_ url: MLApiUrlSchemes, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self where T: MLReqModel, U: MLRespModel {
        sendNetRequest(url: url, method: .post, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T, U>(_ url: MLApiUrlSchemes, paramters: T, reqType: MLNetRequestType, finished: @escaping (_ respModel: U) -> ()) -> Self where T: MLReqModel, U: MLRespModel {
        sendNetRequest(url: url, method: .post, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T: MLReqModel, U: MLRespModel>(_ url: MLApiUrlSchemes, reqType: MLNetRequestType, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, method: .post, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func put<T: MLReqModel, U: MLRespModel>(_ url: MLApiUrlSchemes, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, method: .put, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func put<T: MLReqModel, U: MLRespModel>(_ url: MLApiUrlSchemes, reqType: MLNetRequestType, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, method: .put, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
}

// MARK: - 主线程方法
extension MLNetManager{
    @discardableResult
    public func get<U: MLRespModel>(mainThread url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return get(mainThread: url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func get<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func head<U: MLRespModel>(mainThread url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return head(mainThread: url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func head<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, method: .head, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func delete<U: MLRespModel>(mainThread url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return get(mainThread: url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func delete<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, method: .delete, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, method: .post, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, reqType: MLNetRequestType, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, method: .post, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func put<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, method: .put, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func put<T: MLReqModel, U: MLRespModel>(mainThread url: MLApiUrlSchemes, reqType: MLNetRequestType, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: DispatchQueue.main, method: .put, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
    
    
}

// MARK: - 子线程方法
extension MLNetManager{
    @discardableResult
    public func get<U: MLRespModel>(thread url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return get(thread: url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func get<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func head<U: MLRespModel>(thread url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return head(thread: url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func head<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, method: .head, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func delete<U: MLRespModel>(thread url: MLApiUrlSchemes, finished: @escaping (_ respModel: U) -> ()) -> Self {
        let paramters: MLNetReqModel? = nil
        return delete(thread: url, paramters: paramters, finished: finished)
    }
    
    @discardableResult
    public func delete<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, paramters: T?, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, method: .delete, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, method: .post, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func post<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, reqType: MLNetRequestType, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, method: .post, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func put<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, method: .put, paramters: paramters, finished: finished)
        return self
    }
    
    @discardableResult
    public func put<T: MLReqModel, U: MLRespModel>(thread url: MLApiUrlSchemes, reqType: MLNetRequestType, paramters: T, finished: @escaping (_ respModel: U) -> ()) -> Self {
        sendNetRequest(url: url, queue: netQueue, method: .put, reqType: reqType, paramters: paramters, finished: finished)
        return self
    }
}

// MARK: - 上传文件用
extension MLNetManager {
    public func uploadFiles<U: MLRespModel>(_ url: MLApiUrlSchemes, files: [Data], mimeType: String, paramters: [AnyHashable: Any]?, finished: @escaping (_ respModel: U) -> Void) {
        
        var header = req_headers()
        header.updateValue("multipart/form-data", forKey: "Content-Type")
        guard var urlRequest = try? URLRequest(url: url.getCreditApiUrl(), method: .post, headers: header) else {
            var resultsModel = U()
            resultsModel.code = 3
            resultsModel.msg = MLNetError.result_nil_error(url.getCreditApiUrl()).localizedDescription
            finished(resultsModel)
            return
        }
        
        urlRequest.timeoutInterval = 30
        
        Alamofire.upload(multipartFormData: { (formData) in
            for (i,data) in files.enumerated() {
                var name = UUID().uuidString + "." + mimeType
                formData.append(data, withName: "imageStreams", fileName: name, mimeType: mimeType)
            }
            if let para = paramters {
                for (key, val) in para {
                    if let k = key as? String, let str = val as? String, let data = str.data(using: .utf8) {
                        formData.append(data, withName: k)
                    }
                }
            }
        }, with: urlRequest) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { result in
                    self.json(result, finished: finished)
                }
            case .failure(let encodingError):
                var resultsModel = U()
                resultsModel.code = -2
                resultsModel.msg = encodingError.localizedDescription
                finished(resultsModel)
            }
        }
    }
}


// MARK: - 上传文件用
extension MLNetManager {
    public func uploadImage<U: MLRespModel>(_ url: MLApiUrlSchemes, images: [UIImage], paramters: [AnyHashable: Any]? = nil, finished: @escaping (_ respModel: U) -> Void) {
        
        let mAImg = images.compactMap { (image) -> Data? in
            var imageData = image.jpegData(compressionQuality: 1.0)
            let size = CGFloat((imageData?.count)!)/1024.0/1024
            imageData = image.jpegData(compressionQuality: size >= 1 ? 0.3 : 0.8)
            return imageData
        }
        uploadFiles(url, files: mAImg, mimeType: "image/jpeg", paramters: paramters, finished: finished)
    }
    
}
