//
//  MLReachabilityExt.swift
//

import UIKit
import Alamofire

public enum MLReachabilityExtStatus {
    case unknown
    case notReachable
    case ethernetOrWiFi
    case wwan
}

public class MLReachabilityExt: NSObject {
    
    private var _reachability: NetworkReachabilityManager?

    private static let shared: MLReachabilityExt = MLReachabilityExt()
    
    public class func status() -> MLReachabilityExtStatus {
        return MLReachabilityExt.shared.status()
    }
    
    public class func stop() {
        MLReachabilityExt.shared.stop()
    }
    
    public func status() -> MLReachabilityExtStatus  {
        if _reachability == nil {
            _reachability = NetworkReachabilityManager(host: "www.360.com")
            _reachability?.startListening()
        }
        
        let status = _reachability?.networkReachabilityStatus
        if status == .unknown { return MLReachabilityExtStatus.unknown }
        else if status == .notReachable  { return MLReachabilityExtStatus.notReachable }
        else if status == .reachable(.ethernetOrWiFi) { return MLReachabilityExtStatus.ethernetOrWiFi }
        else if status == .reachable(.wwan) { return MLReachabilityExtStatus.wwan }
        return MLReachabilityExtStatus.unknown
    }
    
    public func stop() {
        guard let reachability = _reachability else { return }
        reachability.stopListening()
    }

}
