//
//  MLSocketManager.swift
//

import UIKit
import SocketIO
import LibBaseSDK

@objcMembers
public class MLSocketManager: NSObject {
    
    fileprivate var manager : SocketManager?
    fileprivate var socket : SocketIOClient?
    
    public static let shared : MLSocketManager = MLSocketManager()
    
    public func socketStart(_ token : String){
        offSocket()
        socket = nil
        
        self.socketStart2("", token);
    }
    
    private func socketStart2(_ url : String, _ token : String){
        let option = ["yunId": token, "additional": "5", "sessionid": token];
        guard let _url = URL.init(string: url) else {return}
        manager = SocketManager(socketURL: _url, config: [.log(false), .compress, .connectParams(option)])
        socket = manager?.defaultSocket
        
        // 监听是否连接上服务器，正确连接走后面的回调
        socket?.on(clientEvent: .connect) {[weak self] (data, ack) in
            Log.i("socket connected")
            self?.socket?.emit("front_logout", with: [])
        }
        socket?.on("front_logout") { (data, ack) in
            #if DEBUG
            Log.i("socket front_logout");
            #endif
            NotificationCenter.default.post(name: Notification.Name.Socket.ForceQuit, object: nil)
        }
        socket?.on(clientEvent: .statusChange) { (data, ack) in
            #if DEBUG
            Log.i("socket statusChange:\(String(describing: self.socket?.status))")
            #endif
        }
        socket?.on(clientEvent: .disconnect) { (data, ack) in
            #if DEBUG
            Log.i("socket what happ")
            #endif
        }
        socket?.on(clientEvent: .error) { (data, ack) in
            #if DEBUG
            Log.i("socket what error")
            #endif
        }
        socket?.connect();
    }
    
    public func offSocket() {
        socket?.disconnect()
        socket?.off(clientEvent: .connect)
        socket?.off("front_logout")
        socket?.off(clientEvent: .statusChange)
        socket?.off(clientEvent: .disconnect)
        socket?.off(clientEvent: .error)
    }
    
    public func keepAlive(){
        let socketData = ["": "serviceAlive"] as [String : Any]
        socket?.emit("message", socketData)
        if socket?.status == .connected {
            #if DEBUG
            Log.i("还活着。。。。。。。。。没有断掉")
            #endif
        }
    }
}
