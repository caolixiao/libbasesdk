//
//  MLURLDownload.h
//
#define kMLDownloadBundle @"ml-company.download"

#import <Foundation/Foundation.h>

typedef void (^MLURLDownloadCallback)(NSDictionary*);
@interface MLURLDownload : NSObject

- (void)download: (NSDictionary*) options completed: (MLURLDownloadCallback) callback;

@end
