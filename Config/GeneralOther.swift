//
//  GeneralOther.swift
//

import Foundation


public var COMPANY_NAME_ZH = "大新华会展控股有限公司"
public var COMPANY_NAME_EN = "Grand China MICE Holdings Co.,Ltd."

public var HasWXURLType: Bool {
    get {
        if let urlTypes = Bundle.main.infoDictionary?["CFBundleURLTypes"] as? [[AnyHashable: Any]] {
            for urlType in urlTypes {
                if let urlName = urlType["CFBundleURLName"] as? String, urlName == "weixin" {
                    return true
                }
            }
        }
        return false
    }
}

