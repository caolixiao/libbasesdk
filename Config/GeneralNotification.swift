//
//  GeneralNotification.swift
//

import Foundation

extension MLCachesNotification.Name {
    public struct Auth {
        public static let Token = MLCachesNotification.Name(rawValue: "ml.toby.caches.name.auth.token")
        public static let Cookie = MLCachesNotification.Name(rawValue: "ml.toby.caches.name.auth.cookie")
        public static let CookieStr = MLCachesNotification.Name(rawValue: "ml.toby.caches.name.auth.cookie.str")
        public static let UserLogin = MLCachesNotification.Name(rawValue: "ml.toby.caches.name.auth.user.login")
        public static let UserId = MLCachesNotification.Name(rawValue: "ml.toby.caches.name.auth.user.id")
    }
}

public struct MLNetNotification {
    public struct Service {
        public static let Sucess = Notification.Name(rawValue: "ml.toby.notification.name.service.sucess")
        public static let Fail = Notification.Name(rawValue: "ml.toby.notification.name.service.fial")
    }
}


extension MLNetNotification {
    public struct Key {
        public static let Service = "ml.toby.notification.key.login"
    }
}



extension Notification.Name {
    public struct Service {
        public static let Sucess = Notification.Name(rawValue: "ml.toby.notification.name.service.sucess")
        public static let Fail = Notification.Name(rawValue: "ml.toby.notification.name.service.fial")
    }
    
    public struct Socket {
        public static let ForceQuit = NSNotification.Name(rawValue: "ml.toby.notification.name.socket.force.quit")
    }
}

extension Notification {
    public struct Key {
        public static let Login = "ml.toby.notification.key.login"
    }
}
