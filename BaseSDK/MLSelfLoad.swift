//
//  MLLoad.swift
//

/// 定义 `protocol`
public protocol SelfLoad: NSObjectProtocol {
    static func load()
}

// 创建代理执行单例
open class NothingToSeeHere {
    static func harmlessFunction() {
        let typeCount = Int(objc_getClassList(nil, 0))
        let  types = UnsafeMutablePointer<AnyClass?>.allocate(capacity: typeCount)
        let autoreleaseintTypes = AutoreleasingUnsafeMutablePointer<AnyClass>(types)
        objc_getClassList(autoreleaseintTypes, Int32(typeCount)) //获取所有的类
        for index in 0 ..< typeCount{
            (types[index] as? SelfLoad.Type)?.load() //如果该类实现了SelfAware协议，那么调用awake方法
        }
        types.deallocate()
//        types.deallocate(capacity: typeCount)
    }
}

/// 执行单例
extension UIApplication {
    private static let runOnce: Void = {
        //使用静态属性以保证只调用一次(该属性是个方法)
        NothingToSeeHere.harmlessFunction()
//        NSObject.harmlessFunction()
    }()
    
    open override var next: UIResponder? {
        UIApplication.runOnce
        return super.next
    }
}


