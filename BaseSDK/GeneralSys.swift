//
//  GeneralSys.swift
//

import Foundation
import AdSupport

// 版本号
public let VERSION      = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
public let BUILDVERSION = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
public let APPNAME      = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String ?? ""
//public let APPNAME      = Bundle.main.infoDictionary?["CFBundleExecutable"] as! String
public let ICONPATH     = (Bundle.main.infoDictionary?["CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] as? [String])?.last as? String ?? ""
public let IMGICON      = UIImage(named: ICONPATH)

public let VERSION_STR  = VERSION.replacingOccurrences(of: ".", with: "")
public let VERSION_INT  = Int(VERSION.replacingOccurrences(of: ".", with: ""))


public let APP_PATH     = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last ?? ""


// bundle Id
public let BUNDLEID: String = Bundle.main.bundleIdentifier ?? ""

// 设备系统版本
public let DEVICE_SYSTEAM_VERSION: String = UIDevice.current.systemVersion
// 设备名称
public let DEVICE_SYSTEAM_NAME: String = UIDevice.current.systemName
// 设备别名-用户定义的名称
public let DEVICE_USER_NAME: String = UIDevice.current.name
// 设备型号
public let DEVICE_MODEL: String = UIDevice.current.model
// 地方型号（国际化区域名称）
public let DEVICE_LOCALIZED_MODEL: String = UIDevice.current.localizedModel


// 设备标示符号
public var DEVICE_UDID: String {
    get {
        guard let strUUID = MLKeychainObject.keychainLoad(BUNDLEID, service: BUNDLEID) as? String else {
            var uuid = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            if uuid == "00000000-0000-0000-0000-000000000000" { uuid = UUID().uuidString; }
            
            //将该uuid保存到keychain
            MLKeychainObject.save(BUNDLEID, service: BUNDLEID, data: uuid as AnyObject)
            return uuid
        }
        return strUUID
    }
}
