//
//  MLKeychainObject.swift
//

import UIKit
import Security

public struct MLKeychainObject {
    
    static public func keychainLoad(_ account: String, service: String)-> AnyObject? {
        var ret: AnyObject? = nil
        var keychainQuery = getKeychainQuery(account, service: service)
        //查询结果返回到 kSecValueData
        keychainQuery.updateValue(kCFBooleanTrue, forKey: kSecReturnData)
        //只返回搜索到的第一条数据
        keychainQuery.updateValue(kSecMatchLimitOne, forKey: kSecMatchLimit)
        var keyData : CFTypeRef? = nil
        //通过条件查询数据
        let status : OSStatus = SecItemCopyMatching(keychainQuery as CFDictionary, &keyData)
        if status == noErr {
            ret = NSKeyedUnarchiver.unarchiveObject(with: keyData as! Data) as AnyObject
        }else {
            #if DEBUG
            Log.i("chain查询失败")
            #endif
        }
        
        return ret;
    }
    
    static public func getKeychainQuery(_ account: String, service: String) -> [AnyHashable : AnyObject] {
        return [
            kSecClass : kSecClassGenericPassword,
            kSecAttrService : service as AnyObject,
            kSecAttrAccount : account as AnyObject,
            kSecAttrAccessible : kSecAttrAccessibleAfterFirstUnlock
        ]
    }
    
    static public func save(_ account: String, service: String, data: AnyObject) {
        var keychainQuery = getKeychainQuery(account, service: service)
        let resultDelete = SecItemDelete(keychainQuery as CFDictionary);
        #if DEBUG
        resultDelete == errSecSuccess ? Log.i("chain删除成功") : Log.i("chain删除失败")
        #endif
        
        keychainQuery.updateValue(NSKeyedArchiver.archivedData(withRootObject: data) as AnyObject, forKey: kSecValueData)
        let result = SecItemAdd(keychainQuery as CFDictionary, nil)
        #if DEBUG
        result == errSecSuccess ? Log.i("chain保存成功") : Log.i("chain保存失败")
        #endif
    }
    
    static public func delete(_ account: String, service: String) {
        let keychainQuery = getKeychainQuery(account, service: service)
        SecItemDelete(keychainQuery as CFDictionary)
    }
    
}
