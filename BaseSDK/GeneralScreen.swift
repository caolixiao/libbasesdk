//
//  GeneralScreen.swift
//

import Foundation

// MARK: -
public let __DEVICE_IPAD: Bool = UIDevice.current.userInterfaceIdiom == .pad
public let __iPhoneX: Bool = IPhoneX
private var IPhoneX: Bool {
    guard #available(iOS 11.0, *) else {
        return false
    }
    return UIApplication.shared.windows[0].safeAreaInsets.bottom > 0
}

// MARK: -
public let __SCREEN_SCALE: CGFloat = UIScreen.main.scale
public let __SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.size.width
public let __SCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.size.height
public let __SCREEN_SIZE: CGSize = UIScreen.main.bounds.size
public let __SCREEN_BOUNDS: CGRect = UIScreen.main.bounds

// 屏幕分辨率
public let __SCREEN_DISPLAY: String = String(format: "%.0fx%.0f", __SCREEN_WIDTH * __SCREEN_SCALE, __SCREEN_HEIGHT * __SCREEN_SCALE)

public let __SCREEN_BOUNDS_SIZE_CHAGE: CGRect = CGRect(x: 0, y: 0, width: __SCREEN_SIZE.height, height: __SCREEN_SIZE.width)

public let __SCALE : CGFloat = (__DEVICE_IPAD ? (__SCREEN_WIDTH / 768) : (__SCREEN_WIDTH / 375))

// 特殊处理
public let CGRectMin = CGRect(x: 0, y: 0, width: 0, height: 0.001)


// MARK: -
public func CGScale(_ __v__ : CGFloat) -> CGFloat { return ceil(((__v__)*(__SCALE))) }
public func CGScaleInt(_ __v__ : CGFloat) -> Int { return Int(CGScale(__v__)) }
public func CGScaleSize(_ __w__ : CGFloat, _ __h__ : CGFloat) -> CGSize  { return CGSize(width: CGScale(__w__), height: CGScale(__h__)) }
public func CGScaleRect(_ __x__ : CGFloat, _ __y__ : CGFloat, _ __w__ : CGFloat, _ __h__ : CGFloat) -> CGRect { return CGRect(x: CGScale(__x__), y: CGScale(__y__), width: CGScale(__w__), height: CGScale(__h__)) }
public func CGScale(centerOfX __v__ : CGFloat) -> CGFloat { return (__SCREEN_WIDTH - CGScale(__v__))*0.5 } // 计算位置居中的x：（__SCREEN_WIDTH-width）*0.5
public func CGScale(marginOfW __v__ : CGFloat) -> CGFloat { return (__SCREEN_WIDTH - CGScale(__v__)*2) } // 计算中间宽度；screenwidth-2*x

//
public func VIEWX(_ __view : UIView) -> CGFloat { return (__view.frame.origin.x) }
public func VIEWY(_ __view : UIView) -> CGFloat { return (__view.frame.origin.y) }
public func VIEWW(_ __view : UIView) -> CGFloat { return (__view.frame.size.width) }
public func VIEWH(_ __view : UIView) -> CGFloat { return (__view.frame.size.height) }
public func VIEWMaxX(_ __view : UIView) -> CGFloat { return (__view.frame.origin.x + __view.frame.size.width) }
public func VIEWMaxY(_ __view : UIView) -> CGFloat { return (__view.frame.origin.y + __view.frame.size.height) }
public func VIEWDiff(_ __val : CGFloat) -> CGFloat { return (__iPhoneX ? CGScale(__val) + __fDiff : CGScale(__val)) }
public func VIEWUnDiff(_ __val : CGFloat) -> CGFloat { return (__iPhoneX ? CGScale(__val) - __fDiff : CGScale(__val)) }

// MARK: - 固定值
@available(iOS 11.0, *)
public let __fHSafeBottom = UIApplication.shared.windows[0].safeAreaInsets.bottom
public let __fHStatusBar: CGFloat = CGFloat(__DEVICE_IPAD ? (__iPhoneX ? 24 : 20) : (__iPhoneX ? 44 : 20))
public let __fHNotStatusNav: CGFloat = CGFloat(__DEVICE_IPAD ? 50 : 44)//ipad navheight=50
public let __fHNav: CGFloat = __fHStatusBar + __fHNotStatusNav
public let __fHTabBar: CGFloat = CGFloat(__DEVICE_IPAD ? (__iPhoneX ? 65 : 50) : (__iPhoneX ? 83:49))
public let __fEdgeTabBarMargin: CGFloat = CGFloat(__DEVICE_IPAD ? (__iPhoneX ? 15 : 0) : (__iPhoneX ? 34 : 0))
public let __FontTitleNav: UIFont = UIFont.boldSystemFont(ofSize: (__DEVICE_IPAD ? 24 : 18))
public let __fHBut = CGScale(42)

public let __fEdge = CGScale(24)
public let __fDiff: CGFloat = CGScale(61)
