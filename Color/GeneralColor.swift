//
//  GeneralColor.swift
//

import Foundation

// MARK: - color tools begin
public func __RGB(_ rgbValue: Int) ->UIColor {
    return UIColor.init(red: CGFloat((rgbValue & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbValue & 0xFF00) >> 8)/255.0, blue: CGFloat(rgbValue & 0xFF)/255.0, alpha: 1)
}
public func __RGBA(_ rgbValue: Int, alpha: CGFloat) ->UIColor {
    return UIColor.init(red: CGFloat((rgbValue & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbValue & 0xFF00) >> 8)/255.0, blue: CGFloat(rgbValue & 0xFF)/255.0, alpha: alpha)
}

public func __COLOR(any: UIColor, dark: UIColor) -> UIColor {
    if #available(iOS 13.0, *) {
        let color: UIColor = UIColor { (traitCollection: UITraitCollection) -> UIColor in
            return traitCollection.userInterfaceStyle == .light ? any : dark
        }
        return color
    } else {
        return  any
    }
}

// MARK: - general color begin
//主题色以及导航颜色
public var color_main: UIColor = { () -> UIColor in return __COLOR(any: __RGB(0xe60012), dark: __RGB(0xe60012)) }()
public let color_nav_bg    = color_f2f2f2
public let color_nav_line  = color_f2f2f2
public let color_nav_title = color_black

//其他相关颜色
public let color_white  = __RGB(0xffffff) // 白色
public let color_black  = __RGB(0x000000) // 黑色
public let color_blue   = __RGB(0x00aaff) // 蓝色
public let color_red    = __RGB(0xfe4343) // 红色
public let color_orange = __RGB(0xffb033) // 橙色
public let color_green  = __RGB(0x2cc17b) // 绿色

/**
 * 用于分割线rgb(236,236,236)
 */
public let color_line = __RGB(0xececec) // 用于分割线rgb(236,236,236) __RGB(0xe0e0e0)//(224,224,224)

/**
*  常用于一级标题文字颜色rgb(42,42,42)
*/
public let color_2a2a2a = __RGB(0x2a2a2a) // 常用于一级标题文字颜色（42,42,42）
/**
 * 用于一级标题文字颜色rgb(51,51,51)
 */
public let color_333333 = __RGB(0x333333) // 一级标题文字颜色(51,51,51)

/**
 * 内容等二级标题文字颜色(147,153,159)
 */
public let color_3e3e3e = __RGB(0x3e3e3e) // 内容等二级标题文字颜色（147,153,159）

/**
 * 三级标题文字颜色(181,185,188)
 */
public let color_b5b9bc = __RGB(0xb5b9bc) // 三级标题文字颜色(181,185,188)

/**
 * 常用于view的默认浅灰背景色（246,246,246）
 */
public let color_f2f2f2 = __RGB(0xf2f2f2) // view的默认浅灰背景色（246,246,246）
/**
 * 部分view的背景色(238,238,238)
 */
public let color_eeeeee = __RGB(0xeeeeee) // 常用于部分view的背景色(238,238,238)

// MARK: - general color end
