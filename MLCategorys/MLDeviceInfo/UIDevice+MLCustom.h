//
//  UIDevice+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIDevice (MLCustom)

+ (NSString *) platform;
+ (NSUInteger) cpuFrequency;
+ (NSUInteger) busFrequency;
+ (NSUInteger) totalMemory;
+ (NSUInteger) userMemory;
+ (NSUInteger) availableMemory;
+ (NSUInteger) wireMemory;
+ (NSUInteger) activeMemory;
+ (NSUInteger) inactiveMemory;
+ (NSUInteger) moreInfo;

@end
