//
//  MLDeviceInfo.m
//


#import "MLDeviceInfo.h"
#import <CoreLocation/CoreLocation.h>

#include <ifaddrs.h>
#include <arpa/inet.h>

#import <SystemConfiguration/CaptiveNetwork.h>
#import <NetworkExtension/NetworkExtension.h>

#import <CommonCrypto/CommonDigest.h>
#import <math.h>


@interface MLDeviceInfo () <CLLocationManagerDelegate>

@end

@implementation MLDeviceInfo

static MLDeviceInfo *sharedInstance = nil;
+ (MLDeviceInfo *) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MLDeviceInfo alloc] init];
    });
    
    return sharedInstance;
}

+ (instancetype) allocWithZone:(struct _NSZone *)zone {
    @synchronized (self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

- (instancetype) init {
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - device 


#pragma mark - wifi 
- (void (^)(NSArray *)) scaneWifi {
    
    NSMutableDictionary* options = [[NSMutableDictionary alloc] init];
    [options setObject:@"EFNEHotspotHelperDemo" forKey: kNEHotspotHelperOptionDisplayName];
    dispatch_queue_t queue = dispatch_queue_create("EFNEHotspotHelperDemo", NULL);
    
    NSLog(@"2.Try");
    BOOL returnType = [NEHotspotHelper registerWithOptions: options queue: queue handler: ^(NEHotspotHelperCommand * cmd) {
        
        NSLog(@"4.Finish");
        NEHotspotNetwork* network;
        if (cmd.commandType == kNEHotspotHelperCommandTypeEvaluate || cmd.commandType == kNEHotspotHelperCommandTypeFilterScanList) {
            // 遍历 WiFi 列表，打印基本信息
            for (network in cmd.networkList) {
                NSString* wifiInfoString = [[NSString alloc] initWithFormat: @"---------------------------\nSSID: %@\nMac地址: %@\n信号强度: %f\nCommandType:%ld\n---------------------------\n\n", network.SSID, network.BSSID, network.signalStrength, (long)cmd.commandType];
                NSLog(@"%@", wifiInfoString);
                
                // 检测到指定 WiFi 可设定密码直接连接
                if ([network.SSID isEqualToString: @"测试 WiFi"]) {
                    [network setConfidence: kNEHotspotHelperConfidenceHigh];
                    [network setPassword: @"123456789"];
                    NEHotspotHelperResponse *response = [cmd createResponse: kNEHotspotHelperResultSuccess];
                    NSLog(@"Response CMD: %@", response);
                    [response setNetworkList: @[network]];
                    [response setNetwork: network];
                    [response deliver];
                }
            }
        }
    }];
    
    return nil;
}

- (NSString *) networkAtKey:(CFStringRef) key {
    NSArray *mAIfa = (__bridge_transfer id)CNCopySupportedInterfaces();
    if (!mAIfa) return nil;
    
    NSString *tmp = @"";
    for (NSString *ifa in mAIfa) {
        NSDictionary *dict = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifa);
        if (dict) { tmp = dict[(__bridge_transfer NSString*)key]; break; }
    }
    return tmp;
}

- (NSString*) BSSID {
    return [self networkAtKey:kCNNetworkInfoKeyBSSID];
}

- (nonnull NSString *) SSID {
    return [self networkAtKey:kCNNetworkInfoKeySSID];
}

- (NSString * _Nullable) SSIDDATA {
    return [self networkAtKey:kCNNetworkInfoKeySSIDData];
}

- (void) setLinkSSID:(NSString *) ssid {
    _linkSSID = [ssid copy];
    
//    NSString *values[] = {ssid};
//    CFArrayRef arrayRef = CFArrayCreate(kCFAllocatorDefault,(void *)values, (CFIndex)1, &kCFTypeArrayCallBacks);
    CFArrayRef arrayRef = (__bridge CFArrayRef)@[ssid];
    if (CNSetSupportedSSIDs(arrayRef)) {
        NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
        CNMarkPortalOffline((__bridge CFStringRef)(ifs[0]));
        NSLog(@"%@", ifs);
    }
}

- (NSString *) ipAdress {
    NSString *address = @"an error occurred when obtaining ip address";
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    
    int success = getifaddrs(&interfaces);
    
    if (0 == success) { // 0 表示获取成功
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if (temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    freeifaddrs(interfaces);
    return address;
}


#pragma mark - info.plist
- (NSString *) appName {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleNameKey];
}

- (NSString *) bundleIdentifier {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleIdentifierKey];
}

- (NSString *) buildVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleVersionKey];
}



@end







