//
//  MLDeviceInfo.h
//


@interface MLDeviceInfo : NSObject

// device
@property(nonatomic,readonly) NSString* systemVersion;
@property(nonatomic,readonly) NSString* deviceName;
@property(nonatomic,readonly) NSString* deviceType;


// wifi
@property (nonatomic, readonly) void (^scaneWifi) (NSArray *mAWifi);

@property (nonatomic, readonly) NSString *SSID;     // wifi Name
@property (nonatomic, readonly) NSString *BSSID;    // wifi mac
@property (nonatomic, readonly) NSString *SSIDDATA; // wifi data
@property (nonatomic, readwrite) NSString *linkSSID;

@property (nonatomic, readonly) NSString *ipAdress;

// info.plist
@property (nonatomic, readonly) NSString *appName;
@property (nonatomic, readonly) NSString *bundleIdentifier;

@property (nonatomic, readonly) NSString *iconPath;
@property (nonatomic, readonly) UIImage *imgIcon;

@property (nonatomic, readonly) NSString *version;
@property (nonatomic, readonly) NSString *versionStr;
@property (nonatomic, readonly) NSString *versionInt;
@property (nonatomic, readonly) NSString *buildVersion;


// system


// local


// screen




@property(nonatomic,readonly) NSString* IDFA;


@property(nonatomic,readonly) NSString* screenDisplay;//1080x1920
@property(nonatomic,readonly) CGSize screenSize;
@property(nonatomic,readonly) NSNumber* deviceScale;
@property(nonatomic,readonly) NSNumber* orientation;
@property(nonatomic,readonly) NSString* carrierType;
@property(nonatomic,readonly) NSNumber* jailBroken;//越狱或破解 0 未越狱，1 越狱
@property(nonatomic,readonly) NSNumber* networkState;//0=Unknown，1=WIFI，2=2G，3=3G，4=4G
//@property(nonatomic,readonly) CLLocationCoordinate2D location;//使用半角逗号隔开
@property(nonatomic,readonly) NSString* language;
@property(nonatomic,assign) BOOL enableLocation;
@property(nonatomic,readonly) NSNumber*screenH;
@property(nonatomic,readonly) NSNumber*screenW;

+ (MLDeviceInfo *) sharedInstance;


@end
