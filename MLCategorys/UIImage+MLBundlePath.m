//
//  UIImage+MLBundlePath.m
//

#import "UIImage+MLBundlePath.h"
#import "NSBundle+MLCustomPath.h"

@implementation UIImage (MLBundlePath)

#pragma mark - load bundle Image
+ (UIImage *) imgNamed:(NSString *) name {
    return [UIImage imgNamed:name path:@"img"];
}

+ (UIImage *) imgGNamed:(NSString *) name {
    return [UIImage imgNamed:name path:@"general"];
}

+ (UIImage *) imgKBNamed:(NSString *) name {
    return [UIImage imgNamed:name path:@"Keyboard"];
}

+ (UIImage *) imgGifNamed:(NSString *) name {
    return [UIImage imgNamed:name path:@"gif"];
}

+ (UIImage *) imgNamed:(NSString *) name path:(NSString *) path {
    if (!name && name.length == 0) return nil;
    
    NSBundle *bundle = [NSBundle mlBundle];
    NSString *img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@@3x", path, name] ofType:@"png"];
    if (!img_path) img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@@2x", path, name] ofType:@"png"];
    if (!img_path) img_path = [bundle pathForResource:[NSString stringWithFormat:@"%@/%@", path, name] ofType:@"png"];
    if (!img_path) NSAssert(true, @"%@", img_path ? img_path : [NSString stringWithFormat:@"没有找到图片:%@", name]);
    return [UIImage imageWithContentsOfFile:img_path];
}

+ (NSArray *) imgsGifNamed:(NSString *) path {
    if (!path) return nil;
    NSBundle *bundle = [NSBundle mlBundle];
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/gif/", bundle.resourcePath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *testDirectory = [documentsDirectory stringByAppendingPathComponent:path];
    NSArray *array = [fileManager contentsOfDirectoryAtPath:testDirectory error:NULL];
    NSMutableArray *__array = [NSMutableArray array];
    for (NSString *str in array) {
        if ([str isEqualToString:@".DS_Store"] || ![str hasSuffix:@"@2x.png"]) continue;
        
        NSString *tmp = [NSString stringWithFormat:@"%@/%@", testDirectory, [str stringByReplacingOccurrencesOfString:@"@3x" withString:@""]];
        [__array addObject:[UIImage imageWithContentsOfFile:tmp]];
    }
    
    return __array.count == 0 ? nil : __array;
}

@end
