//
//  NSObject+MLSpaceTool.m
//

#import "NSObject+MLSpaceTool.h"
#include <sys/param.h>
#include <sys/mount.h>

@implementation NSObject (MLSpaceTool)

//手机剩余空间
+ (NSString *) freeDiskSpaceInBytes{
    /// 剩余大小
    float freesize = 0.0;
    /// 是否登录
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    if (dictionary)
    {
        NSNumber *_free = [dictionary objectForKey:NSFileSystemFreeSize];
        freesize = [_free unsignedLongLongValue]*1.0;

    }
    return [self humanReadableStringFromBytes:freesize];
//    NSLog(@"...%f",freesize);
//
//    struct statfs buf;
//    long long freespace = -1;
//    if(statfs("/var", &buf) >= 0){
//        freespace = (long long)(buf.f_bsize * buf.f_bfree);
//    }
//    return [self humanReadableStringFromBytes:freespace];
    
}
//手机总空间
+ (NSString *) totalDiskSpaceInBytes
{
//    struct statfs buf;
//    long long freespace = 0;
//    if (statfs("/", &buf) >= 0) {
//        freespace = (long long)buf.f_bsize * buf.f_blocks;
//    }
//    if (statfs("/private/var", &buf) >= 0) {
//        freespace += (long long)buf.f_bsize * buf.f_blocks;
//    }
//    printf("%lld\n",freespace);
//    return [self humanReadableStringFromBytes:freespace];
    
    float freesize = 0.0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    if (dictionary) {
        NSNumber *_free = [dictionary objectForKey:NSFileSystemSize];
        freesize = [_free unsignedLongLongValue]*1.0;
    }
    return [self humanReadableStringFromBytes:freesize];
}

//遍历文件夹获得文件夹大小
+ (NSString *) folderSizeAtPath:(NSString*) folderPath {
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    return [self humanReadableStringFromBytes:folderSize];
}

//单个文件的大小
+ (long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}

//计算文件大小
+ (NSString *)humanReadableStringFromBytes:(unsigned long long)byteCount {
    float numberOfBytes = byteCount;
    int multiplyFactor = 0;
    
    NSArray *tokens = [NSArray arrayWithObjects:@"bytes",@"KB",@"MB",@"GB",@"T",@"P",@"E" ,@"Z",@"Y",nil];
    
    while (numberOfBytes > 1024) {
        numberOfBytes /= 1024;
        multiplyFactor++;
    }
    
    if (multiplyFactor == 0) return @"0 KB";
    if (multiplyFactor == 1) {
        return [NSString stringWithFormat:@"%4.0f %@",numberOfBytes, [tokens objectAtIndex:multiplyFactor]];
    } else if (multiplyFactor == 2) {
        return [NSString stringWithFormat:@"%4.1f %@",numberOfBytes, [tokens objectAtIndex:multiplyFactor]];
    }
    return [NSString stringWithFormat:@"%4.2f %@",numberOfBytes, [tokens objectAtIndex:multiplyFactor]];
}

@end
