//
//  NSObject+MLSpaceTool.h
//

#import <Foundation/Foundation.h>

@interface NSObject (MLSpaceTool)

+ (NSString *) freeDiskSpaceInBytes;
+ (NSString *) totalDiskSpaceInBytes;
+ (NSString *) folderSizeAtPath:(NSString*) folderPath;
+ (NSString *) humanReadableStringFromBytes:(unsigned long long) byteCount;

@end
