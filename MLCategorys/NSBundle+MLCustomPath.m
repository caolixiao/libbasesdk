//
//  NSBundle+MLCustomPath.m
//

#import "NSBundle+MLCustomPath.h"


static NSString *__mlBundle_name__ = @"MLBundle";

@implementation NSBundle (MLCustomPath)
    
+ (void) setBundleName:(NSString *) name {
    __mlBundle_name__ = name;
}
    
+ (NSBundle *) mlBundle {
    return [NSBundle mlBundleWithName:__mlBundle_name__ atFramework:nil];
}
    
+ (NSBundle *) mlBundleWithName:(NSString *) name atFramework:(NSString *) framework {
    NSString *path = [[NSBundle  mainBundle] pathForResource:name ofType:@"bundle"];
    
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    if(bundle==nil && framework){
        for (NSBundle *_bundle in [NSBundle allFrameworks]) {
            if([_bundle.resourcePath hasSuffix:framework]) {
                path = [_bundle pathForResource:name ofType:@"bundle"];
                bundle  =[NSBundle bundleWithPath:path];
                break;
            }
        }
    }
    return bundle;
}
    
+ (NSString *) pathNIB:(NSString *) name {
    return [[[NSBundle mlBundle] bundlePath] stringByAppendingFormat:@"/%@.nib", name];
}
    
+ (NSData *) dataNIB:(NSString *) name {
    return [NSData dataWithContentsOfFile:[NSBundle pathNIB:name]];
}
    
+ (NSString *) pathHtml:(NSString *) name {
    return [[[NSBundle mlBundle] bundlePath] stringByAppendingFormat:@"/html/%@.html", name];
}
    
+ (NSData *) dataHtml:(NSString *) name {
    return [NSData dataWithContentsOfFile:[NSBundle pathHtml:name]];
}

@end
