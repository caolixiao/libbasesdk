//
//  NSBundle+MLCustomPath.h
//

#import <Foundation/Foundation.h>
#import "UIImage+MLBundlePath.h"

@interface NSBundle (MLCustomPath)

+ (void) setBundleName:(NSString *) name;
+ (NSBundle *) mlBundle;
+ (NSBundle *) mlBundleWithName:(NSString *) name atFramework:(NSString *) framework;
    
+ (NSString *) pathNIB:(NSString *) name;
+ (NSData *) dataNIB:(NSString *) name;

+ (NSString *) pathHtml:(NSString *) name;
+ (NSData *) dataHtml:(NSString *) name;

@end
