//
//  UIImage+MLBundlePath.h
//

#import <UIKit/UIKit.h>

@interface UIImage (MLBundlePath)

#pragma mark - load bundle Image
+ (UIImage *) imgNamed:(NSString *) name;
+ (UIImage *) imgGNamed:(NSString *) name;
+ (UIImage *) imgKBNamed:(NSString *) name;
+ (UIImage *) imgNamed:(NSString *) name path:(NSString *) path;

+ (UIImage *) imgGifNamed:(NSString *) name;
+ (NSArray *) imgsGifNamed:(NSString *) path;

@end
