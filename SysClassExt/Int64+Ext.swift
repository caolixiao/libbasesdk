//
//  Int64.swift
//

import UIKit

extension Int64 {
    public func strDecimal() -> String {
        let format = NumberFormatter()
        format.numberStyle = .decimal
        return "¥" + (format.string(from: NSNumber(value: self)) ?? "")
    }
}


extension Double {
    public func strDecimal() -> String {
        let format = NumberFormatter()
        format.numberStyle = .decimal
        format.positivePrefix = ""
        format.maximumFractionDigits = 2
        format.minimumFractionDigits = 2
        format.numberStyle = .currency
        return format.string(from: NSNumber(value: self)) ?? ""
    }
    
    public func strPrefixDecimal() -> String {
        let format = NumberFormatter()
        format.numberStyle = .decimal
        //        format.positiveFormat = "###,###.##" //设置格式
        format.positivePrefix = "¥"
        format.maximumFractionDigits = 2 //设置小数点后最多3位
        format.minimumFractionDigits = 2 //设置小数点后最少2位（不足补0）
        format.numberStyle = .currency
        return format.string(from: NSNumber(value: self)) ?? ""
    }
}
