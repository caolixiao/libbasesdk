//
//  String+Ext.swift
//

import UIKit
import CommonCrypto

extension String {
    
    public func fw(_ fontSize: CGFloat, height: CGFloat = CGScale(15)) -> CGFloat {
        let font = UIFont.systemFont(ofBaseSize: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        let rect = NSString(string: self).boundingRect(with: CGSize(width: CGFloat(MAXFLOAT), height: height), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(rect.width)
    }
    
    public func fh(_ fontSize: CGFloat, width: CGFloat) -> CGFloat {
        let font = UIFont.systemFont(ofBaseSize: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        let rect = NSString(string: self).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(rect.height)
    }
    
//    - (CGFloat) heightAttributesWithSize:(CGFloat) fs width:(CGFloat) width {
//        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:fs]};
//        CGRect rect = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
//                                         options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
//                                      attributes:attributes
//                                         context:nil];
//        return rect.size.height + 2.0f;
//    }
//    
    public func date(format: String = "yyyy-MM-dd HH:mm:ss", outFormat: String = "yyyy-MM-dd") -> String {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = format
        guard let date = dfmatter.date(from: self) else { return "" }
        dfmatter.dateFormat = outFormat
        return dfmatter.string(from: date)
    }
    
    

    /// 从url中获取后缀 例：.pdf
    public var pathExtension: String {
        return (self as NSString).pathExtension
    }
    
    /// ls
    public var lastPathComponent: String {
        return (self as NSString).lastPathComponent
    }
    
    /// 删除.ml
    public var stringByDeletingPathExtension: String {
        return (self as NSString).deletingPathExtension
    }
    
    /// cd ..
    public var deletingLastPathComponent: String {
        return (self as NSString).deletingLastPathComponent
    }
    
    /// md5
    public var md5Hash: String {
        if let str = self.cString(using: .utf8) {
            let strLen = CUnsignedInt(self.lengthOfBytes(using: .utf8))
            let digestLen = Int(CC_MD5_DIGEST_LENGTH)
            let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
            CC_MD5(str, strLen, result)
            
            let hash = NSMutableString()
            for i in 0 ..< digestLen {
                hash.appendFormat("%02x", result[i])
            }
            result.deinitialize(count: digestLen)
            return String(format: hash as String)
        }
        return ""
    }
}





