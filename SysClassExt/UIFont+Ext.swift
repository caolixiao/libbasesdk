//
//  UIFont+Ext.swift
//

import UIKit

extension UIFont {
    
    // 获取指定大小的系统字体
    @objc
    public class func systemFont(ofBaseSize fontSize: CGFloat) -> UIFont {
        let size = fontSize * __SCALE
        return UIFont.systemFont(ofSize: size)
    }
    
    // 获取指定大小和粗细的系统字体
    public class func systemFont(ofBaseSize fontSize: CGFloat, weight: CGFloat) -> UIFont {
        let size = fontSize * __SCALE
        let w = UIFont.Weight(rawValue: weight * __SCALE)
        return UIFont.systemFont(ofSize: size, weight: w)
    }
    
    //获得指定大小的粗体系统字体
    @objc
    public class func boldSystemFont(ofBaseSize fontSize: CGFloat) -> UIFont {
        let size = fontSize * __SCALE
        return UIFont.boldSystemFont(ofSize: size)
    }
    
    // 获取指定大小的斜体系统字体
    public class func italicSystemFont(ofBaseSize fontSize: CGFloat) -> UIFont {
        let size = fontSize * __SCALE
        return UIFont.italicSystemFont(ofSize: size)
    }
    
    // 获取指定大小和粗细的系统字体，并且其中的数字字符间距相等
    public class func monospacedDigitSystemFont(ofBaseSize fontSize: CGFloat, weight: CGFloat) -> UIFont {
        let size = fontSize * __SCALE
        let w = UIFont.Weight(rawValue: weight * __SCALE)
        return UIFont.monospacedDigitSystemFont(ofSize: size, weight: w)
    }
    
    // 获取指定大小和粗细的系统字体，并且其中的数字字符间距相等
    @available(iOS 13.0, *)
    open class func monospacedSystemFont(ofBaseSize fontSize: CGFloat, weight: CGFloat) -> UIFont {
        let size = fontSize * __SCALE
        let w = UIFont.Weight(rawValue: weight * __SCALE)
        return UIFont.monospacedSystemFont(ofSize: size, weight: w)
    }    
}
