//
//  FileManager+Ext.swift
//

import UIKit

extension FileManager {

    /// 移动道指定目录
    @discardableResult
    public func move(srcPath: String, destPath: String) -> Bool {
        if !FileManager.default.fileExists(atPath: srcPath) {
            return true
        }

        do {
            if (FileManager.default.fileExists(atPath: destPath)) {
                try FileManager.default.removeItem(atPath: destPath)
            }
            
            try FileManager.default.copyItem(atPath: srcPath, toPath: destPath)
        } catch {
            Log.i("move caches file fail ------- \n  \(srcPath) \n \(destPath)")
            return false
        }
        
        return true
    }

}
