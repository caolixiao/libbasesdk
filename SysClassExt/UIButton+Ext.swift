//
//  UIButton+Ext.swift
//

import UIKit
import Alamofire
import AlamofireImage

extension UIButton {
    
    public func setImageUrl(_ apiUrl: MLApiUrlSchemes) {
        if let url = URL(string: apiUrl.getCreditApiUrl()) {
            self.af_setImage(for: .normal, url: url, completion: { (resp: DataResponse<UIImage>) in
                MLNetManager.shared.resp_cookie(resp)
            })
        }
    }
}
