//
//  UIImageView+Ext.swift
//

import UIKit
import AlamofireImage

extension UIImageView {
    
    public func setImageUrl(_ url: String) {
        if let url = URL(string: url) {
            self.af_setImage(withURL: url)
        }
    }
    
    public func setImageUrl(_ apiUrl: MLApiUrlSchemes) {
        if let url = URL(string: apiUrl.getCreditApiUrl()) {
            self.af_setImage(withURL: url)
        }
    }
    
    public func setImageUrl(_ apiUrl: MLApiUrlSchemes, image: UIImage) {
        if let url = URL(string: apiUrl.getCreditApiUrl()) {
            self.af_setImage(withURL: url, placeholderImage: image)
        }
    }
    

//    public func save(image: UIImage) -> String {
//        
//    }
//    /**
//     *  @brief 保存缓冲到指定位置
//     *  @param img 图片数据
//     *  @return 返回保存的路径
//     */
//    + (NSString *) saveImage:(UIImage *) img {
//        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
//        NSString *name = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
//
//        NSData *imgData = UIImageJPEGRepresentation(img, 0.5);
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/thumbCache"];
//        NSString *imgPath = [path stringByAppendingFormat:@"/%@.png", name];
//        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
//        [fileManager createFileAtPath:imgPath contents:imgData attributes:nil];
//
//        return imgPath;
//    }

    
}
