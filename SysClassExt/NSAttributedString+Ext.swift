//
//  NSAttributedString+Ext.swift
//

import UIKit

extension NSAttributedString {
    
    public enum ImgLocal {
        case left
        case right
    }
    
    /// 图片加到字符串里边【替换字符串里边的图片】
    public func replace(imageNamed: String, local: ImgLocal = .right, tintColor: UIColor? = nil, bounds: CGRect = CGRect(x: 0, y: 0, width: 14, height: 14)) -> NSAttributedString  {
        let attachment = NSTextAttachment(data: nil, ofType: nil)
        if let tinit = tintColor {
            attachment.image = UIImage(named: imageNamed)?.tint(with: tinit)
        } else {
            attachment.image = UIImage(named: imageNamed)
        }
        attachment.bounds = bounds
        
        let attrStr = NSAttributedString(attachment: attachment)
        guard let this: NSMutableAttributedString = self.mutableCopy() as? NSMutableAttributedString else { return self }
        if local == .right {
            this.replaceCharacters(in: NSMakeRange(this.length - 1, 1), with: attrStr)
        } else if local == .left {
            this.replaceCharacters(in: NSMakeRange(0, 1), with: attrStr)
        }
        return this
    }
    
    /// 设置字体的高度
    public class func setTitle(height: Int = 6, font: UIFont = UIFont.systemFont(ofBaseSize: 13), str: String?) -> NSAttributedString {
        guard let val = str else { return NSMutableAttributedString(string: "") }
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        style.lineSpacing = CGFloat(height)
        style.lineBreakMode = NSLineBreakMode.byTruncatingTail
        let mutaAttrStr = NSMutableAttributedString(string: val)
        mutaAttrStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, val.count))
        mutaAttrStr.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, val.count))
        return mutaAttrStr
    }
    
    // MARK: - heigth
    public func fH(width: CGFloat) -> CGFloat {
        let size = CGSize(width: width, height:  CGFloat(MAXFLOAT))
        let rect = self.boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesDeviceMetrics, .usesFontLeading, .truncatesLastVisibleLine], context: nil)
        return ceil(rect.size.height)
    }
    
    // 根据给定的范围计算宽高，如果计算宽度，则请把宽度设置为最大，计算高度则设置高度为最大
    ///
    /// - Parameters:
    ///   - width: 宽度的最大值
    ///   - height: 高度的最大值
    /// - Returns: 文本的实际size
    public func fH(width: CGFloat, height: CGFloat) -> CGFloat {
        let attributed = self
        let framesetter = CTFramesetterCreateWithAttributedString(attributed)
//        let size = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRange(location: 0, length: attributed.length), nil, rect.size, nil)
        let size = CTFramesetterSuggestFrameSizeForAttributedStringWithConstraints(framesetter: framesetter, length: attributed.length, size: CGSize(width: width, height: height))
        return size.height + 1
    }
    
    public func CTFramesetterSuggestFrameSizeForAttributedStringWithConstraints(framesetter: CTFramesetter, length: Int, size: CGSize, numberOfLines: Int = 10000) -> CGSize {
        
        var range = CFRangeMake(0, length)
        var constraints = size
        
        if numberOfLines == 1 {
            constraints = CGSize(width: 10000, height: 10000)
        }
        else if numberOfLines > 0 {
            let rect = CGRect(x: 0, y: 0, width: Int(constraints.width), height: numberOfLines)
            var path = CGPath(rect: rect, transform: nil)
            
            let frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), path, nil)
            let lines = CTFrameGetLines(frame)
            
            if CFArrayGetCount(lines) > 0 {
                let lastVisibleLineIndex = min(numberOfLines, CFArrayGetCount(lines)) - 1
                let lastVisibleLine = CFArrayGetValueAtIndex(lines, lastVisibleLineIndex)
                if let line = lastVisibleLine?.load(as: CTLine.self) {
                    let rangeToLayout = CTLineGetStringRange(line)
                    
                    range = CFRangeMake(0, rangeToLayout.length + rangeToLayout.location)
                }
            }
        }
    
       let suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, range, nil, constraints, nil)
       return CGSize(width: ceil(suggestedSize.width), height: ceil(suggestedSize.height))
    }
    
    public func getImageRunFrame(run: CTRun, lineOringinPoint: CGPoint, offsetX: CGFloat) -> CGRect {
        /// 计算位置 大小
        var runBounds = CGRect.zero
        var h: CGFloat = 0
        var w: CGFloat = 0
        var x: CGFloat = 0
        var y: CGFloat = 0
        
        var asecnt: CGFloat = 0
        var descent: CGFloat = 0
        var leading: CGFloat = 0
        
        
        let cfRange = CFRange.init(location: 0, length: 0)
        
        w = CGFloat(CTRunGetTypographicBounds(run, cfRange, &asecnt, &descent, &leading))
        h = asecnt + descent + leading
        /// 获取具体的文字距离这行原点的距离 || 算尺寸用的
        x = offsetX + lineOringinPoint.x
        /// y
        y = lineOringinPoint.y - descent
        runBounds = CGRect.init(x: x, y: y, width: w, height: h)
        return runBounds
    }
}
