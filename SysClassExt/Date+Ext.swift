//
//  Date+Ext.swift
//

import UIKit

extension Date {
    
    /// 获取当前时间 eg. 2020-09-09
    public func strOfyyyyMMdd() -> String {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd"
        return dfmatter.string(from: self)
    }
    
    /// 获取 前几天或者后几天的时间  eg. -1 昨天 1 明天
    public func strOfDay(dateFormat: String = "yyyy-MM-dd", day: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        let newTime = TimeInterval((24*60*60) * day)
        let newDate = self.addingTimeInterval(newTime)
        return dateFormatter.string(from: newDate)
    }
    
    /// 格式当前时间
    public func string(dateFormat: String = "yyyy-MM-dd") -> String {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = dateFormat
        return dfmatter.string(from: self)
    }
    
    public static func string(dateFormat: String = "yyyy-MM-dd") -> String {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = dateFormat
        return dfmatter.string(from: Date())
    }
    
    public static func updateTimeToCurrennTime(timeStamp: Double) -> String {
        //获取当前的时间戳
        let currentTime = Date().timeIntervalSince1970
        //时间戳为毫秒级要 ／ 1000， 秒就不用除1000，参数带没带000
        let timeSta:TimeInterval = TimeInterval(timeStamp / 1000)
        //时间差
        let reduceTime : TimeInterval = currentTime - timeSta
        //时间差小于60秒
        if reduceTime <= 0 {
            return "0秒前"
        }
        if reduceTime < 60 {
            return "\(Int(reduceTime))秒前"
        }
        //时间差大于一分钟小于60分钟内
        let mins = Int(reduceTime / 60)
        if mins < 60 {
            return "\(mins)分钟前"
        }
        let hours = Int(reduceTime / 3600)
        if hours < 24 {
            return "\(hours)小时前"
        }
        //        let days = Int(reduceTime / 3600 / 24)
        //        if days < 30 {
        //            return "\(days)天前"
        //        }
        //不满足上述条件---或者是未来日期-----直接返回日期
        let date = NSDate(timeIntervalSince1970: timeSta)
        let dfmatter = DateFormatter()
        //yyyy-MM-dd HH:mm:ss
        dfmatter.dateFormat="yyyy-MM-dd HH:mm"
        return dfmatter.string(from: date as Date)
    }
}
