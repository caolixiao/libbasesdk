//
//  Array+Ext.swift
//

import UIKit

extension Array {
    public mutating func modifyForEach(_ body: (_ index: Index, _ element: inout Element) -> ()) {
        for index in indices {
            modifyElement(atIndex: index) { body(index, &$0) }
        }
    }

    public mutating func modifyElement(atIndex index: Index, _ modifyElement: (_ element: inout Element) -> ()) {
        var element = self[index]
        modifyElement(&element)
        self[index] = element
    }
    
    @inlinable
    public mutating func forEachRet(_ body: (Element) throws -> Any?) rethrows -> Any? {
        var val: Any? = nil
        for element in self {
            if let ret = try body(element), val == nil { val = ret }
        }
        return val
    }
}
