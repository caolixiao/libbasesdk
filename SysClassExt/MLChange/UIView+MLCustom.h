//
//  UIView+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIView (MLCustom)

#pragma mark - UIWindow
+ (UIWindow *_Nonnull) getTopWindow;

#pragma mark - UIViewController 
- (UIViewController * _Nullable) viewController;
- (UIViewController * _Nullable) currentViewController;

#pragma mark -
- (int) getSubviewIndex;

- (void) bringToFront;
- (void) sendToBack;

- (void) bringOneLevelUp;
- (void) sendOneLevelDown;

- (BOOL) isInFront;
- (BOOL) isAtBack;

- (void) swapDepthsWithView:(UIView * _Nullable) swapView;

#pragma mark -
@property(nullable, copy) NSArray *colors;
@property(nullable, copy) NSArray<NSNumber *> *locations;
@property CGPoint startPoint;
@property CGPoint endPoint;

+ (UIView *_Nullable)gradientViewWithColors:(NSArray<UIColor *> *_Nullable)colors locations:(NSArray<NSNumber *> *_Nullable)locations startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

- (void)setGradientBackgroundWithColors:(NSArray<UIColor *> *_Nullable)colors locations:(NSArray<NSNumber *> *_Nullable)locations startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

@end
