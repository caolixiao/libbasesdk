//
//  NSString+MLCustom.m
//

#import "NSString+MLCustom.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (MLCustom)

#pragma mark - 
- (NSString *) chineseToPinyin {
    if (!self && self.length == 0) return nil;
    NSMutableString *source = [self mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)source, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)source, NULL, kCFStringTransformStripDiacritics, NO);
    return source;
}

#pragma mark -
- (NSString *) md5Hash {
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (int)strlen(cStr), result);
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]];
}

#pragma mark -
- (NSString *) encodeURL {
        NSCharacterSet *encodeUrlSet = [NSCharacterSet URLQueryAllowedCharacterSet];
        return [self stringByAddingPercentEncodingWithAllowedCharacters:encodeUrlSet];
}

- (NSString *) encodeURIComponent {
        return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                     (CFStringRef)self,
                                                                                     NULL,
                                                                                     (CFStringRef)@"!*'();:@&=+$,/?%#[]\" ",
                                                                                     kCFStringEncodingUTF8));
}

- (NSString *) urlPercentEscapedString {
    static NSString * const kAFCharactersGeneralDelimitersToEncode = @":#[]@"; // does not include "?" or "/" due to RFC 3986 - Section 3.4
    static NSString * const kAFCharactersSubDelimitersToEncode = @"!$&'()*+,;=";
    
    NSMutableCharacterSet * allowedCharacterSet = [[NSCharacterSet URLQueryAllowedCharacterSet] mutableCopy];
    [allowedCharacterSet removeCharactersInString:[kAFCharactersGeneralDelimitersToEncode stringByAppendingString:kAFCharactersSubDelimitersToEncode]];
    
    static NSUInteger const batchSize = 50;
    
    NSUInteger index = 0;
    NSMutableString *escaped = @"".mutableCopy;
    
    while (index < self.length) {
        NSUInteger length = MIN(self.length - index, batchSize);
        NSRange range = NSMakeRange(index, length);
        
        // To avoid breaking up character sequences such as 👴🏻👮🏽
        range = [self rangeOfComposedCharacterSequencesForRange:range];
        
        NSString *substring = [self substringWithRange:range];
        NSString *encoded = [substring stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacterSet];
        [escaped appendString:encoded];
        
        index += range.length;
    }
    
    return escaped;
}

#pragma mark - time
+ (NSDate *) convertDateFromString:(NSString*) uiDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (uiDate.length == 19) [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    else if (uiDate.length == 16) [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    else if (uiDate.length == 10) [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [formatter dateFromString:uiDate];
    return date;
}

+ (NSString *) lunarCalendarWithDate:(NSDate *) date {
    NSArray *mAYears = @[@"甲子", @"乙丑", @"丙寅", @"丁卯",  @"戊辰",  @"己巳",  @"庚午",  @"辛未",  @"壬申",  @"癸酉",
                         @"甲戌",   @"乙亥",  @"丙子",  @"丁丑", @"戊寅",   @"己卯",  @"庚辰",  @"辛己",  @"壬午",  @"癸未",
                         @"甲申",   @"乙酉",  @"丙戌",  @"丁亥",  @"戊子",  @"己丑",  @"庚寅",  @"辛卯",  @"壬辰",  @"癸巳",
                         @"甲午",   @"乙未",  @"丙申",  @"丁酉",  @"戊戌",  @"己亥",  @"庚子",  @"辛丑",  @"壬寅",  @"癸丑",
                         @"甲辰",   @"乙巳",  @"丙午",  @"丁未",  @"戊申",  @"己酉",  @"庚戌",  @"辛亥",  @"壬子",  @"癸丑",
                         @"甲寅",   @"乙卯",  @"丙辰",  @"丁巳",  @"戊午",  @"己未",  @"庚申",  @"辛酉",  @"壬戌",  @"癸亥"];
    NSArray *mAMonths = @[@"正月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月", @"九月", @"十月", @"冬月", @"腊月"];
    NSArray *mADays = @[@"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",
                        @"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"二十",
                        @"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十"];
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierChinese];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *comp = [calendar components:unitFlags fromDate:date];
    
    NSString *year = [mAYears objectAtIndex:(comp.year - 1)];
    NSString *month = [NSString stringWithFormat:@"%@%@", comp.leapMonth ? @"润" : @"", [mAMonths objectAtIndex:(comp.month - 1)]];
    NSString *day = [mADays objectAtIndex:(comp.day - 1)];
    
    return [NSString stringWithFormat:@"%@年%@%@", year, month, day];
}

+ (NSString *) lunarCalendarMonthWithDate:(NSDate *) date {
    return [[NSString lunarCalendarWithDate:date] substringFromIndex:3];
}


+ (NSString *) monthWithDate:(NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM"];
    NSArray *month = @[@"一月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月", @"九月", @"十月", @"十一月", @"十二月"];
    return [month objectAtIndex:([[dateFormatter stringFromDate:date] intValue] - 1)];
}

+ (NSString *) weekWithDate:(NSDate *) date {
    NSDateComponents *componets = [[NSCalendar autoupdatingCurrentCalendar] components:NSWeekdayCalendarUnit fromDate:date];
    NSArray *weeks = @[@"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六"];
    //NSArray *weeks = @[@"星期日", @"星期一", @"星期二", @"星期三", @"星期四", @"星期五", @"星期六"];
    return [weeks objectAtIndex:([componets weekday] - 1)];
}

+ (NSString *) dayWithDate:(NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *) lunarCalendarString {
    return [NSString lunarCalendarWithDate:[NSDate new]];
}

+ (NSString *) lunarCalendarMonthString {
    return [NSString lunarCalendarMonthWithDate:[NSDate new]];
}

+ (NSString *) dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSString *) dDayString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSString *) monthString {
    return [NSString monthWithDate:[NSDate date]];
}

+ (NSString *) weekString {
    return [NSString weekWithDate:[NSDate date]];
}

+ (NSString *) dayString {
    return [NSString dayWithDate:[NSDate date]];
}

+ (NSString *) sssString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYYMMddHHmmssSSS"];
    return [dateFormatter stringFromDate:[NSDate date]];
}


#pragma mark -
- (NSString *)trimmedString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *) delHtmlFormat {
    NSRegularExpression *regularExpretion=[NSRegularExpression regularExpressionWithPattern:@"<[^>]*>|\n" options:0 error:nil];
    return [regularExpretion stringByReplacingMatchesInString:self options:NSMatchingReportProgress range:NSMakeRange(0, self.length) withTemplate:@""];
}

- (NSData*) JSONString {
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:NSJSONReadingAllowFragments | NSJSONReadingMutableContainers |NSJSONReadingMutableLeaves error:&error];
    if (error != nil) return nil;
    return result;
}

#pragma mark -
- (NSMutableDictionary*) queryStringDictionary {
    @try {
        NSArray *list = [self componentsSeparatedByString:@"&"];
        NSMutableDictionary *val = [NSMutableDictionary dictionaryWithCapacity:list.count];
        for(NSString *e in list) {
            NSArray *content = [e componentsSeparatedByString:@"="];
            [val setObject:[content objectAtIndex:1] forKey:[content objectAtIndex:0]];
        }
        return val;
    } @catch (NSException *exception) {
        return nil;
    }
}

- (NSString *) stringFromElemnt {
    NSRange range1=[self rangeOfString:@"\"" options:NSCaseInsensitiveSearch];
    NSRange range2=[self rangeOfString:@"&" options:NSCaseInsensitiveSearch];
    if(range1.location == NSNotFound && range2.location == NSNotFound) return self;
    
    if (range1.location != NSNotFound && range2.location != NSNotFound) {
        if (range1.location < range2.location) return [self substringToIndex:range1.location];
        else return [self substringToIndex:range2.location];
    }
    
    if (range2.location != NSNotFound) return [self substringToIndex:range2.location];
    if (range1.location != NSNotFound) return [self substringToIndex:range1.location];
    
    return nil;
}

- (NSString *) stringWithDict:(NSDictionary *) dict {
    NSMutableString *str = [NSMutableString stringWithCapacity:0];
    for (NSString *key in dict.allKeys) {
        [str appendFormat:@"%@=%@&", key, dict[key]];
    }
    if (str.length > 0) [str deleteCharactersInRange:NSMakeRange(str.length - 1, 1)];
    return str;
}


#pragma mark -
- (long long) findNumFromString
{
    if (!self) return 0;
    NSMutableString *numString = [[NSMutableString alloc] init];
    NSString *tempStr;
    
    NSScanner *scanner = [NSScanner scannerWithString:self];
    NSCharacterSet *nums = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    while (![scanner isAtEnd]) {
        [scanner scanUpToCharactersFromSet:nums intoString:NULL];
        
        [scanner scanCharactersFromSet:nums intoString:&tempStr];
        [numString appendString:tempStr];
        tempStr = @"";
    }
    
    long long num = [numString integerValue];
    
    return num;
}

- (float) findFloatFromString
{
    if (!self) return -1;
    NSMutableString *fString = [[NSMutableString alloc] init];
    NSString *tempStr;
    
    NSScanner *scanner = [NSScanner scannerWithString:self];
    NSCharacterSet *nums = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    
    while (![scanner isAtEnd]) {
        [scanner scanUpToCharactersFromSet:nums intoString:NULL];
        
        [scanner scanCharactersFromSet:nums intoString:&tempStr];
        [fString appendString:tempStr];
        tempStr = @"";
    }
    
    return  [fString floatValue];
}

- (NSString *)getPureNumber{
    NSError *error;
    // 创建NSRegularExpression对象并指定正则表达式
    NSString * reg = @"[0-9]|-";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:reg
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSMutableString * phone = [NSMutableString string];
    if (!error) { // 如果没有错误
        // 获取特特定字符串的范围
        NSArray * match = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
        if (match) {
            // 截获特定的字符串
            for (int i =0 ;i < match.count ;i++) {
                NSTextCheckingResult * result = match[i];
                NSRange range = [result range];
                NSString *mStr = [self substringWithRange:range];
                [phone appendString:mStr];
            }
            NSLog(@"截获特定的字符串 = %@",phone);
        }
    } else { // 如果有错误，则把错误打印出来
        NSLog(@"error - %@", error);
    }
    
    return phone;
}

@end
