//
//  UIImage+MLCustom.m
//

#import "UIImage+MLCustom.h"
#import <LibBaseSDK/LibBaseSDK-Swift.h>

@implementation UIImage (MLCustom)

#pragma mark - size
- (CGSize) imgSize {
    CGFloat fw = CGImageGetWidth(self.CGImage);
    CGFloat fh = CGImageGetHeight(self.CGImage);
    return CGSizeMake(fw, fh);
}

- (CGFloat) scale_w_h {
    CGFloat fw = CGImageGetWidth(self.CGImage);
    CGFloat fh = CGImageGetHeight(self.CGImage);
    return fh/fw;
}

- (CGFloat) scale_h_w {
    CGFloat fw = CGImageGetWidth(self.CGImage);
    CGFloat fh = CGImageGetHeight(self.CGImage);
    return fh/fw;
}

#pragma mark -
+ (UIImage *)imageWithView:(UIView *) view {
//    UIGraphicsBeginImageContext(view.bounds.size);
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - size
+ (UIImage *) scaleImage:(UIImage *) image {
    CGFloat sourceWidth  = image.size.width;
    CGFloat sourceHeight = image.size.height;
    
    CGFloat viewWidth  = UIScreen.mainScreen.bounds.size.width * UIScreen.mainScreen.scale;
    CGFloat scale = viewWidth / sourceWidth;
    
    CGFloat outWidth  = scale > 0 ? viewWidth : sourceWidth;
    CGFloat outHeight = scale > 0 ? sourceHeight * scale : sourceHeight;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(outWidth, outHeight), NO, UIScreen.mainScreen.scale);
    [image drawInRect:CGRectMake(0, 0, outWidth, outHeight)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

+ (UIImage *)scaleImage:(UIImage *)image toScale:(float) scaleSize {
    CGFloat width = ((int)(image.size.width * scaleSize *2.0))/2.0;
    CGFloat height = ((int)(image.size.height * scaleSize *2.0))/2.0;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, UIScreen.mainScreen.scale);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

#pragma mark - size
- (UIImage *) scaleAuto {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    if(width > [UIScreen mainScreen].bounds.size.height)
        width = [UIScreen mainScreen].bounds.size.height;
    CGFloat scale = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 1 : width / 375.0;
    return [UIImage scaleImage:self toScale: scale];
}

- (UIImage *) rescaleImageToSize:(CGSize) size {
    CGRect rect = (CGRect){CGPointZero, size};
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    [self drawInRect:rect];
    UIImage *resImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resImage;
}

- (UIImage *) scaleImageToSize:(CGSize) size {
    CGFloat sourceWidth  = self.size.width;
    CGFloat sourceHeight = self.size.height;
    
    CGFloat viewWidth  = size.width;
    CGFloat viewHeight = size.height;
    
    CGFloat outWidth  = viewWidth;
    CGFloat outHeight = viewHeight;
    
    CGFloat widthFactor = sourceWidth / viewWidth;
    CGFloat heightFactor = sourceHeight / viewHeight;
    
    CGFloat scaleFactor = widthFactor > heightFactor ? widthFactor : heightFactor;
    outWidth =  sourceWidth / scaleFactor;
    outHeight =  sourceHeight / scaleFactor;
    
    return [self rescaleImageToSize: CGSizeMake(outWidth, outWidth)];
}

#pragma mark - color
+ (UIImage *)imageWithColor:(UIColor *)color {
    return [UIImage imageWithColor:color size:CGSizeMake(1, 1)];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *) imageWithAlpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
    CGRect bounds = (CGRect){CGPointZero, self.size};
    [self drawInRect:bounds blendMode:kCGBlendModeOverlay alpha:alpha];
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

- (UIImage *) tintWithColor:(UIColor *) tintColor {
    return [self tintWithColor:tintColor size: self.size];
}

- (UIImage *) tintWithColor:(UIColor *) tintColor size:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    
    [tintColor setFill];
    CGRect bounds = (CGRect){CGPointZero, size};
    UIRectFill(bounds);
    
    [self drawInRect: bounds blendMode: kCGBlendModeDestinationIn alpha:1.0f];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

void ProviderReleaseData (void *info, const void *data, size_t size) {
    free((void*)data);
}

- (UIImage*) imageBlackToTransparentTintColor:(UIColor *) tintColor {
    // 分配内存
    const int imageWidth = self.size.width;
    const int imageHeight = self.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), self.CGImage);
    
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++)
    {
        if ((*pCurPtr & 0x81bd2500) == 0x81bd2500)    // 将白色变成透明
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 230; //0~255
            ptr[2] = 115;
            ptr[1] = 111;
        }
        else if ((*pCurPtr & 0xFFFFFFff) == 0xffffffff)    // 将白色变成透明
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 255;
            ptr[2] = 255;
            ptr[1] = 255;
        }
        else
        {
            // 改成下面的代码，会将图片转成想要的颜色
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[0] = 0;
            //            ptr[3] = 230; //0~255
            //            ptr[2] = 115;
            //            ptr[1] = 111;
            
        }
        
    }
    
    // 将内存转成image
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    
    // 释放
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    // free(rgbImageBuf) 创建dataProvider时已提供释放函数，这里不用free
    
    return resultUIImage;
}

#pragma mark - setting
- (UIImage *)clipImageToSize:(CGSize)size scale:(BOOL)scale{
    CGFloat sourceWidth  = self.size.width;
    CGFloat sourceHeight = self.size.height;
    
    CGFloat viewWidth  = size.width;
    CGFloat viewHeight = size.height;
    
    CGFloat outWidth  = viewWidth;
    CGFloat outHeight = viewHeight;
    
    CGFloat widthFactor = sourceWidth / viewWidth;
    CGFloat heightFactor = sourceHeight / viewHeight;
    
    CGFloat scaleFactor = widthFactor<heightFactor?widthFactor:heightFactor;
    
    outWidth  =  viewWidth * scaleFactor;
    outHeight =  viewHeight * scaleFactor;
    
    CGRect rect = CGRectMake((sourceWidth -outWidth )/2, (sourceHeight -outHeight )/2, outWidth,outHeight);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();//获取当前quartz 2d绘图环境
    
    CGContextTranslateCTM(currentContext, 0-rect.origin.x,sourceHeight-rect.origin.y);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    CGContextClipToRect( currentContext, rect);//设置当前绘图环境到矩形框
    
    CGRect _rect = CGRectMake(0.0, 0.0f, sourceWidth,sourceHeight);
    
    CGContextDrawImage(currentContext, _rect, self.CGImage);//绘图
    
    UIImage *_clipImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    UIImage *_image;
    
    if(scale){
        CGRect srect = CGRectMake(0.0, 0.0, viewWidth, viewHeight);
        UIGraphicsBeginImageContext(srect.size);
        [_clipImage drawInRect:srect];
        _image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }else{
        _image=_clipImage;
    }
    
    return [_image fixOrientation:self.imageOrientation];
}

-(UIImage *)fixOrientation:(UIImageOrientation)_imageOrientation{
    
    // No-op if the orientation is already correct
    if (_imageOrientation == UIImageOrientationUp) return self;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (_imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (_imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (_imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [[UIImage alloc] initWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (UIImage *) cornerImageToRadius:(int) radius margin:(int) margin marginColor:(UIColor *) clolor {
    CGFloat viewWidth  = self.size.width;
    CGFloat viewHeight = self.size.height;
    
    CGRect rect = CGRectMake(0, 0, viewWidth,viewHeight);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGRect rectToDraw = CGRectInset(rect, 0, 0);
    
    UIBezierPath *borderPath = [UIBezierPath bezierPathWithRoundedRect:rectToDraw byRoundingCorners:(UIRectCorner)UIRectCornerAllCorners cornerRadii:CGSizeMake(radius, radius)];
    
    /* Background */
    CGContextSaveGState(ctx);
    {
        CGContextAddPath(ctx, borderPath.CGPath);
        
        CGContextSetFillColorWithColor(ctx, clolor.CGColor);
        
        CGContextDrawPath(ctx, kCGPathFill);
    }
    CGContextRestoreGState(ctx);
    {
        
        CGRect _rect=CGRectMake(rect.origin.x+margin, rect.origin.y+margin, rect.size.width-2*margin, rect.size.height-2*margin);
        CGRect rectToDraw = CGRectInset(_rect, margin, margin);
        
        UIBezierPath *iconPath = [UIBezierPath bezierPathWithRoundedRect:rectToDraw byRoundingCorners:(UIRectCorner)UIRectCornerAllCorners cornerRadii:CGSizeMake(radius, radius)];
        
        /* Image and Clip */
        CGContextSaveGState(ctx);
        {
            CGContextAddPath(ctx, iconPath.CGPath);
            CGContextClip(ctx);
            
            CGContextTranslateCTM(ctx, 0.0, self.size.height);
            CGContextScaleCTM(ctx, 1.0, -1.0);
            
            CGContextDrawImage(ctx, _rect, self.CGImage);
            
        }
        
    }
    
    UIImage *_clipImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    return _clipImage;
}

#pragma mark - image change
+ (NSString *) image2String:(UIImage *) image {
    NSData* pictureData = UIImageJPEGRepresentation(image,0.3); // 进行图片压缩从0.0到1.0（0.0表示最大压缩，质量最低);
    NSString* pictureDataString = [pictureData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return pictureDataString;
}

+ (UIImage *) string2Image:(NSString *) string {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *image = [UIImage imageWithData:data];
    return image;
}

//static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth,float ovalHeight){
//    float fw, fh;
//    if (ovalWidth == 0 || ovalHeight == 0) {
//        CGContextAddRect(context, rect);
//        return;
//    }
//    CGContextSaveGState(context);//这是保存的么？
//    CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
//    CGContextScaleCTM(context, ovalWidth, ovalHeight);
//    fw = CGRectGetWidth(rect) / ovalWidth;
//    fh = CGRectGetHeight(rect) / ovalHeight;
//    
//    CGContextMoveToPoint(context, fw, fh/2);  // Start at lower right corner
//    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);  // Top right corner
//    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1); // Top left corner
//    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1); // Lower left corner
//    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); // Back to lower right
//    
//    CGContextClosePath(context);
//    CGContextRestoreGState(context);
//}
//
//
//+ (id) createRoundedRectImage:(UIImage*)image size:(CGSize)size{
//    // the size of CGContextRef
//    int w = size.width;
//    int h = size.height;
//    
//    UIImage *img = image;
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
//    CGRect rect = CGRectMake(0, 0, w, h);
//    
//    CGContextBeginPath(context);
//    addRoundedRectToPath(context, rect, 10, 10);
//    CGContextClosePath(context);
//    CGContextClip(context);
//    CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
//    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
//    UIImage *result = [UIImage imageWithCGImage:imageMasked];
//    CGContextRelease(context);
//    CGColorSpaceRelease(colorSpace);
//    CGImageRelease(imageMasked);
//    return result;
//}
//
//+ (UIImage *) scaleImage:(UIImage *)image toWidth:(int)toWidth toHeight:(int)toHeight{
//    
//    CGSize size = CGSizeMake(width, height);
//    UIGraphicsBeginImageContext(size);
//    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
//    image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    CGSize subImageSize = CGSizeMake(toWidth, toHeight);
//    CGRect subImageRect = CGRectMake(x, y, toWidth, toHeight);
//    CGImageRef imageRef = image.CGImage;
//    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, subImageRect);
//    UIGraphicsBeginImageContext(subImageSize);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextDrawImage(context, subImageRect, subImageRef);
//    UIImage* subImage = [UIImage imageWithCGImage:subImageRef];
//    CGImageRelease(subImageRef);
//    UIGraphicsEndImageContext();
//    return subImage;
//}
//
//
//+ (NSData *)scaleData:(NSData *) imageData toWidth:(int)toWidth toHeight:(int)toHeight{
//    UIImage *image = [[UIImage alloc] initWithData:imageData];
//    int width=0;
//    int height=0;
//    int x=0;
//    int y=0;
//    
//    if (image.size.width<toWidth){
//        width = toWidth;
//        height = image.size.height*(toWidth/image.size.width);
//        y = (height - toHeight)/2;
//    }else if (image.size.height<toHeight){
//        height = toHeight;
//        width = image.size.width*(toHeight/image.size.height);
//        x = (width - toWidth)/2;
//    }else if (image.size.width>toWidth){
//        width = toWidth;
//        height = image.size.height*(toWidth/image.size.width);
//        y = (height - toHeight)/2;
//    }else if (image.size.height>toHeight){
//        height = toHeight;
//        width = image.size.width*(toHeight/image.size.height);
//        x = (width - toWidth)/2;
//    }else{
//        height = toHeight;
//        width = toWidth;
//    }
//    
//    CGSize size = CGSizeMake(width, height);
//    UIGraphicsBeginImageContext(size);
//    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
//    image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    CGSize subImageSize = CGSizeMake(toWidth, toHeight);
//    CGRect subImageRect = CGRectMake(x, y, toWidth, toHeight);
//    CGImageRef imageRef = image.CGImage;
//    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, subImageRect);
//    UIGraphicsBeginImageContext(subImageSize);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextDrawImage(context, subImageRect, subImageRef);
//    UIImage* subImage = [UIImage imageWithCGImage:subImageRef];
//    CGImageRelease(subImageRef);
//    UIGraphicsEndImageContext();
//    NSData *data = UIImageJPEGRepresentation(subImage, 1.0);
//    return data;
//}
//
//+ (CGRect) rect: (CGSize ) size {
//    int width=0;
//    int height=0;
//    int x=0;
//    int y=0;
//    if (size.width<toWidth){
//        width = toWidth;
//        height = image.size.height*(toWidth/size.width);
//        y = (height - toHeight)/2;
//    }else if (image.size.height<toHeight){
//        height = toHeight;
//        width = image.size.width*(toHeight/size.height);
//        x = (width - toWidth)/2;
//    }else if (image.size.width>toWidth){
//        width = toWidth;
//        height = image.size.height*(toWidth/size.width);
//        y = (height - toHeight)/2;
//    }else if (image.size.height>toHeight){
//        height = toHeight;
//        width = image.size.width*(toHeight/size.height);
//        x = (width - toWidth)/2;
//    }else{
//        height = toHeight;
//        width = toWidth;
//    }
//    
//    return CGRectMake(x, y, width, height);
//}

@end
