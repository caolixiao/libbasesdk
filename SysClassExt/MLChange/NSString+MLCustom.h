//
//  NSString+MLCustom.h
//

#import <Foundation/Foundation.h>

@interface NSString (MLCustom)

#pragma mark -
- (NSString *) chineseToPinyin;

#pragma mark -
- (NSString *) md5Hash;

#pragma mark -
- (NSString *) encodeURL;
- (NSString *) encodeURIComponent;
- (NSString *) urlPercentEscapedString;

#pragma mark - time
+ (NSDate *) convertDateFromString:(NSString*) uiDate;

+ (NSString *) lunarCalendarWithDate:(NSDate *) date;  //XX年MMdd (农历)
+ (NSString *) lunarCalendarMonthWithDate:(NSDate *) date; //MMdd (农历)
+ (NSString *) monthWithDate:(NSDate *) date;
+ (NSString *) weekWithDate:(NSDate *) date;
+ (NSString *) dayWithDate:(NSDate *) date;

+ (NSString *) lunarCalendarString;      //XX年MMdd (农历)
+ (NSString *) lunarCalendarMonthString; //MMdd
+ (NSString *) dateString; // yyyy-MM-dd HH:mm:ss
+ (NSString *) dDayString; // yyyy年MM月dd日
+ (NSString *) yearString;
+ (NSString *) monthString;
+ (NSString *) weekString;
+ (NSString *) dayString;
+ (NSString *) sssString;


#pragma mark -
- (NSData*) JSONString;
- (NSString *) trimmedString;
- (NSString *) delHtmlFormat;

#pragma mark -
- (NSString *) stringFromElemnt;
- (NSMutableDictionary*) queryStringDictionary;
- (NSString *) stringWithDict:(NSDictionary *) dict;

#pragma mark -
- (long long) findNumFromString;
- (float) findFloatFromString;

- (NSString *) getPureNumber;// 去掉号码后面的文字，获取号码

@end
