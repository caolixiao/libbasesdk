//
//  UIImage+MLCustom.h
//

#import <UIKit/UIKit.h>

@interface UIImage (MLCustom)

#pragma mark - size
// 获取图片的尺寸
@property (nonatomic, assign, readonly) CGSize imgSize;
@property (nonatomic, assign, readonly) CGFloat scale_w_h;
@property (nonatomic, assign, readonly) CGFloat scale_h_w;

#pragma mark -
// 毛玻璃

// view 转化成图片 UIView => UIImage
+ (UIImage *) imageWithView:(UIView *) view;

#pragma mark - size
// 对图片大小进行缩放
+ (UIImage *) scaleImage:(UIImage *) image;
+ (UIImage *) scaleImage:(UIImage *) image toScale:(float) scaleSize;

- (UIImage *) scaleAuto;
- (UIImage *) rescaleImageToSize:(CGSize) size; // 图片大小重定义
- (UIImage *) scaleImageToSize:(CGSize) size;   // 图片比例优先

#pragma mark - color
// 创建一个颜色图片
+ (UIImage *) imageWithColor:(UIColor *) color;      // 1X1
+ (UIImage *) imageWithColor:(UIColor *) color size:(CGSize)size;

// 修改图片的透明度
- (UIImage *) imageWithAlpha:(CGFloat) alpha;

// 修改图片的颜色
- (UIImage *) tintWithColor:(UIColor *) tintColor;
- (UIImage *) tintWithColor:(UIColor *) tintColor size:(CGSize)size;

// 特殊图片处理
- (UIImage*) imageBlackToTransparentTintColor:(UIColor *) tintColor;


#pragma mark - setting
- (UIImage *) clipImageToSize:(CGSize)size scale:(BOOL) scale;    // 图片裁剪并且缩放
- (UIImage *) cornerImageToRadius:(int) radius margin:(int) margin marginColor:(UIColor *) clolor; //图片圆角、边框

#pragma mark - image change
// 图片转换为字符串/字符串转换为图片
+ (NSString *) image2String:(UIImage *) image;
+ (UIImage *) string2Image:(NSString *) string;

#pragma mark -
// TODO: 暂时没有实现
/*
 * 缩放图片
 * image 图片对象
 * toWidth 宽
 * toHeight 高
 * return 返回图片对象
 */
+ (UIImage *) scaleImage:(UIImage *) image toWidth:(int) toWidth toHeight:(int) toHeight;

/*
 * 缩放图片数据
 * imageData 图片数据
 * toWidth 宽
 * toHeight 高
 * return 返回图片数据对象
 */
+ (NSData *) scaleData:(NSData *) imageData toWidth:(int) toWidth toHeight:(int) toHeight;

/*
 * 圆角
 * image 图片对象
 * size 尺寸
 */
+ (id) createRoundedRectImage:(UIImage*) image size:(CGSize) size;

@end
