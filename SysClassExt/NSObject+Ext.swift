//
//  NSObject+Ext.swift
//

import UIKit

extension NSObject {    
    
    
    
    //计算一个字符串数组，左对齐，宽度不定的高度
    //itemWids每个控件的宽度int
    //itemHei  每个控件固定高度
    //所在控件于父视图viewLeft，viewRight左右距离
    //margin:两个控件之间的距离
    //marginCol 两个控件上下间距
    //marginRow 两个控件左右距离
    open class func getItemsHeight(itemWids:[Int],itemHei:CGFloat,viewLeft:Int = CGScaleInt(15),viewRight:Int = CGScaleInt(15),marginCol:CGFloat,marginRow:CGFloat) -> Int {
        var selfHeight = 0
        if itemWids.count == 0 {
            return selfHeight
        }
        
        var rowX:CGFloat = 0
        var colY:CGFloat = 0
        let viewWid:CGFloat = __SCREEN_SIZE.width-CGFloat(viewLeft+viewRight)
        
        for i in 0..<itemWids.count {
            let itemW = CGFloat(itemWids[i])
            if i>0 {
                let item1 = itemWids[i]
                let nextItem = rowX + CGFloat(item1)
                if nextItem > viewWid {
                    if rowX == 0 {
                        
                    }else {
                        colY = colY + itemHei + marginCol
                    }
                    rowX = 0
                }
            }
            //下一个label的left
            rowX = rowX + itemW + marginRow
        }
        selfHeight = Int(ceil(colY + itemHei))
        return selfHeight
    }
}

extension NSObject {
    private struct AssociatedKey {
        static var objc = "ml.toby.object.objc"
        static var anyParam = "ml.toby.object.param.any"
        static var intParam = "ml.toby.object.param.int"
        static var int64Param = "ml.toby.object.param.int64"
        static var boolParam = "ml.toby.object.param.bool"
        static var stringParam = "ml.toby.object.param.string"
        static var arrayParam = "ml.toby.object.param.array"
        static var dictionaryParam = "ml.toby.object.param.dictionary"
    }
    
    public var objc: Any? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.objc) as? Any }
        set { objc_setAssociatedObject(self, &AssociatedKey.objc, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var param: Any? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.anyParam) as? Any }
        set { objc_setAssociatedObject(self, &AssociatedKey.anyParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var intParam: Int? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.intParam) as? Int }
        set { objc_setAssociatedObject(self, &AssociatedKey.intParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var int64Param: Int64? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.intParam) as? Int64 }
        set { objc_setAssociatedObject(self, &AssociatedKey.intParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    public var boolParam: Bool? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.stringParam) as? Bool }
        set { objc_setAssociatedObject(self, &AssociatedKey.stringParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var stringParam: String? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.stringParam) as? String }
        set { objc_setAssociatedObject(self, &AssociatedKey.stringParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var arrayParam: Array<Any>? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.arrayParam) as? Array }
        set { objc_setAssociatedObject(self, &AssociatedKey.arrayParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var dictionaryparam: Dictionary<AnyHashable, Any>? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.dictionaryParam) as? Dictionary }
        set { objc_setAssociatedObject(self, &AssociatedKey.dictionaryParam, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
}
